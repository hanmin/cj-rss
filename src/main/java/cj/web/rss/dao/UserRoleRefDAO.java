package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.UserRoleRef;

/**
 * 
 * @author 
 *
 */
@DAO
public interface UserRoleRefDAO{
	
	@SQL("select * from UserRoleRef where userId=:1")
	List<UserRoleRef> queryId(int userId);
	
    @SQL("INSERT INTO UserRoleRef (id,roleId,userId) VALUES (:t.id,:t.roleId,:t.userId)")
    void create(@SQLParam("t") UserRoleRef userRoleRef);
	
    @SQL("delete from UserRoleRef where userId=:1")
    void deleteById(int userId);
}
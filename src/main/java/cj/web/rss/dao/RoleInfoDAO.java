package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.RoleInfo;

/**
 * 
 * @author 
 *
 */
@DAO
public interface RoleInfoDAO{
	
	@SQL("SELECT * FROM RoleInfo WHERE 1=1  AND name=:t.name, AND descs=:t.descs, AND deleteFlag=:t.deleteFlag, AND createUser=:t.createUser, AND createAt=:t.createAt, AND updateUser=:t.updateUser, AND updateAt=:t.updateAt ORDER BY updateAt DESC LIMIT :t.start,:t.end ")
    List<RoleInfo> query(@SQLParam("t") RoleInfo roleInfo);
    
    @SQL("select * from RoleInfo where id=:1")
    RoleInfo queryById(int id);
    
    @ReturnGeneratedKeys
    @SQL("INSERT INTO RoleInfo (id,name,descs,deleteFlag,createUser,createAt,updateUser,updateAt) VALUES (:t.id,:t.name,:t.descs,:t.deleteFlag,:t.createUser,:t.createAt,:t.updateUser,:t.updateAt)")
    public int create(@SQLParam("t") RoleInfo roleInfo);
	
	@SQL("UPDATE RoleInfo SET name=:t.name,descs=:t.descs,updateUser=:t.updateUser,updateAt=:t.updateAt WHERE id=:t.id")
    void update(@SQLParam("t") RoleInfo roleInfo);
    
    @SQL("UPDATE RoleInfo SET deleteFlag=0 WHERE id=:1")
    void deleteById(int id);

    @SQL("select id,name,descs from RoleInfo where deleteFlag = 1  #if(:t.name != ''){ and name like '%##(:t.name)%' } order by updateAt desc limit :t.start,:t.length")
	List<RoleInfo> getList(@SQLParam("t")RoleInfo role);
    
    @SQL("select id,name,descs from RoleInfo where deleteFlag = 1 ")
	List<RoleInfo> getListAll();

    @SQL("select count(1) from RoleInfo where deleteFlag = 1  #if(:t.name != ''){ and name like '%##(:t.name)%' } ")
	int getListSize(@SQLParam("t")RoleInfo role);

    @SQL("select count(1) from RoleInfo where deleteFlag = 1 and name=:1")
	int queryByName(String name);
}
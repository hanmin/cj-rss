package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.MemuInfo;

/**
 * 
 * @author 
 *
 */
@DAO
public interface MemuInfoDAO{
	
	@SQL("SELECT * FROM MemuInfo WHERE deleteFlag=1 AND parentId=:t.parentId, AND name=:t.name, AND url=:t.url, AND descs=:t.descs, AND deleteFlag=:t.deleteFlag, AND createUser=:t.createUser, AND createAt=:t.createAt, AND updateUser=:t.updateUser, AND updateAt=:t.updateAt ORDER BY updateAt DESC LIMIT :t.start,:t.end ")
    List<MemuInfo> query(@SQLParam("t") MemuInfo MemuInfo);
    
    @SQL("select * from MemuInfo where id=:1")
    MemuInfo queryById(int id);
    
    @SQL("INSERT INTO MemuInfo (parentId,name,url,descs,deleteFlag,createUser,createAt,updateUser,updateAt) VALUES (:t.parentId,:t.name,:t.url,:t.descs,:t.deleteFlag,:t.createUser,:t.createAt,:t.updateUser,:t.updateAt)")
    void create(@SQLParam("t") MemuInfo MemuInfo);
	
	@SQL("UPDATE MemuInfo SET parentId=:t.parentId,name=:t.name,url=:t.url,descs=:t.descs,updateUser=:t.updateUser,updateAt=:t.updateAt WHERE id=:t.id")
    void update(@SQLParam("t") MemuInfo MemuInfo);
    
    @SQL("UPDATE MemuInfo SET deleteFlag=0 WHERE id=:1")
    void deleteById(int id);

    @SQL("select m.id,m.parentId,m.name,m.url,m.descs,a.name as parent_name FROM MemuInfo m left join MemuInfo a ON m.parentId = a.id where m.deleteFlag = 1 #if(:t.name!=''){ and m.name like '%##(:t.name)%' } order by m.parentId,m.updateAt limit :t.start,:t.length")
	List<MemuInfo> getList(@SQLParam("t") MemuInfo menu);

    @SQL("select count(1) from MemuInfo where deleteFlag=1 #if(:t.name!=''){ and name like '%##(:t.name)%' }")
	int getListSize(@SQLParam("t") MemuInfo menu);

    @SQL("select * from MemuInfo where deleteFlag=1")
	List<MemuInfo> getAll();
}
package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.RoleMenuRef;

/**
 * 
 * @author 
 *
 */
@DAO
public interface RoleMenuRefDAO{
	
	@SQL("SELECT * FROM RoleMenuRef WHERE 1=1  AND roleId=:t.roleId  AND menuId=:t.menuId ORDER BY update_date DESC LIMIT :t.start,:t.end ")
    List<RoleMenuRef> query(@SQLParam("t") RoleMenuRef roleMenuRef);
    
    @SQL("select * from RoleMenuRef where id=:1")
    RoleMenuRef queryById(int id);
    
    @SQL("INSERT INTO RoleMenuRef (roleId,menuId) VALUES (:t.roleId,:t.menuId)")
    void create(@SQLParam("t") RoleMenuRef roleMenuRef);
	
	@SQL("UPDATE RoleMenuRef SET roleId=:t.roleId,menuId=:t.menuId WHERE id=:t.id")
    void update(@SQLParam("t") RoleMenuRef roleMenuRef);
    
    @SQL("UPDATE RoleMenuRef SET deleteFlag=0 WHERE id=:1")
    void deleteById(int id);

    @SQL("select menuId from RoleMenuRef r where r.roleId = :1")
	List<String> getMenuIdsByRoleId(int roleId);

    @SQL("delete from RoleMenuRef where roleId = :1")
	void deleteByRoleId(int roleId);
}
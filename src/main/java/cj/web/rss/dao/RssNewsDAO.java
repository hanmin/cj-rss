package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;
import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.RssNews;

/**
 * 
 * @author 
 *
 */
@DAO
public interface RssNewsDAO{
	
	@SQL("SELECT id,title,evacuate,link,source,pubDate,updateDate,productName,productId,videoImage,videoUrl,author,deleteFlag,createAt,updateAt FROM RssNews WHERE deleteFlag=1 #if(:t.title != ''){ and title like '%##(:t.title)%' } ORDER BY updateDate DESC LIMIT :t.start,:t.length ")
    List<RssNews> query(@SQLParam("t") RssNews rssNews);
	
	@SQL("SELECT count(1) FROM RssNews WHERE deleteFlag=1 #if(:t.title != ''){ and title like '%##(:t.title)%' } ")
    int querysize(@SQLParam("t") RssNews rssNews);
	
	@SQL("select * from RssNews where id=:1")
	RssNews queryById(int id);
    
    @SQL("select id,title,evacuate,link,source,pubDate,updateDate,productName,productId,videoImage,videoUrl,author,deleteFlag,createAt,updateAt from RssNews where link=:1 and updateDate=:2 and deleteFlag=1")
    RssNews queryByLink(String link,String updateDate);
    
    @SQL("select count(1) from RssNews where link=:1 and updateDate=:2 and deleteFlag=1")
    int queryByLinkSize(String link,String updateDate);
    
    @SQL("INSERT INTO RssNews (title,evacuate,link,description,source,pubDate,updateDate,productName,productId,videoImage,videoUrl,author,deleteFlag,createAt,updateAt) VALUES (:t.title,:t.evacuate,:t.link,:t.description,:t.source,:t.pubDate,:t.updateDate,:t.productName,:t.productId,:t.videoImage,:t.videoUrl,:t.author,1,now(),now())")
    void create(@SQLParam("t") RssNews rssNews);
	
    @SQL("UPDATE RssNews SET deleteFlag=0 WHERE link=:1")
    void deleteById(String link);
}
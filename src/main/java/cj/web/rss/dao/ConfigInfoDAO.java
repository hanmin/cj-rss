package cj.web.rss.dao;
/*
 *  
 *  
*/
import java.util.List;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;
import cj.web.rss.domain.ConfigInfo;

/**
 * 
 * @author 
 *
 */
@DAO
public interface ConfigInfoDAO{
	
	@SQL("SELECT id,parentId,descs,code,name,value,orderNo,deleteFlag,createUser,date_format(createAt,'%Y-%m-%d %H:%i:%s') as createAt,updateUser,date_format(updateAt,'%Y-%m-%d %H:%i:%s') as updateAt FROM ConfigInfo WHERE deleteFlag=1 #if(:t.id!=''){and (id like '%##(:t.id)%' or parentId like '%##(:t.parentId)%' or code like '%##(:t.code)%' or name like '%##(:t.name)%') }  ORDER BY updateAt DESC LIMIT :t.start,:t.length ")
    List<ConfigInfo> query(@SQLParam("t") ConfigInfo configInfo);
	
	@SQL("SELECT count(id) FROM ConfigInfo WHERE deleteFlag=1 #if(:t.id!=''){and (id like '%##(:t.id)%' or parentId like '%##(:t.parentId)%' or code like '%##(:t.code)%' or name like '%##(:t.name)%') } ")
    int querysize(@SQLParam("t") ConfigInfo configInfo);
    
    @SQL("select * from ConfigInfo where id=:1")
    ConfigInfo queryById(int id);
    
    @SQL("INSERT INTO ConfigInfo (parentId,code,name,value,orderNo,descs,deleteFlag,createUser,createAt,updateUser,updateAt) VALUES (:t.parentId,:t.code,:t.name,:t.value,:t.orderNo,:t.descs,:t.deleteFlag,:t.createUser,now(),:t.updateUser,now())")
    void create(@SQLParam("t") ConfigInfo configInfo);
	
	@SQL("UPDATE ConfigInfo SET descs=:t.descs,parentId=:t.parentId,code=:t.code,name=:t.name,value=:t.value,orderNo=:t.orderNo,updateUser=:t.updateUser,updateAt=now() WHERE id=:t.id")
    void update(@SQLParam("t") ConfigInfo configInfo);
    
    @SQL("UPDATE ConfigInfo SET deleteFlag=0 WHERE id=:1")
    void deleteById(int id);
}
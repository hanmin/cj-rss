package cj.web.rss.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.var.Model;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cj.utils.Constants;
import cj.utils.DateUtils;
import cj.web.rss.constants.MemuEnum;
import cj.web.login.controllers.BaseController;
import cj.web.rss.dao.MemuInfoDAO;
import cj.web.rss.dao.RoleInfoDAO;
import cj.web.rss.dao.RoleMenuRefDAO;
import cj.web.rss.domain.MemuInfo;
import cj.web.rss.domain.RoleInfo;
import cj.web.rss.domain.RoleMenuRef;

@Path("/roleInfo")
public class RoleInfoController extends BaseController {

	private static Logger logger = Logger.getLogger(RoleInfoController.class.getName());

	@Autowired
	private RoleInfoDAO roleInfoDao;

	@Autowired
	private MemuInfoDAO menuInfoDao;

	@Autowired
	private RoleMenuRefDAO roleMenuRefDAO;

	@Get("query")
	public String query(Model model, RoleInfo roleInfo) {
		roleInfoDao.query(roleInfo);
		return null;
	}

	@Get("queryById")
	public String queryById(Model model, @Param("id") int id) {
		roleInfoDao.queryById(id);
		return null;
	}

	@Get("show")
	public String show(Model model) {
		//MemuEnum
        model.add("MemuEnum",MemuEnum.jsgl);
		return "/views/user/roleInfoList.vm";
	}

	@Post("getList")
	public String getList(RoleInfo role) {
		List<RoleInfo> arr = roleInfoDao.getList(role);

		JSONObject jo = new JSONObject();

		int size = roleInfoDao.getListSize(role);

		jo.put("data", arr);
		jo.put("draw", role.getDraw());
		jo.put("recordsTotal", size);
		jo.put("recordsFiltered", size);

		return "@" + jo.toString();
	}

	@Post("roleRefMenuInit")
	public String roleRefMenuInit(@Param("menuIds") String menuIds) {
		JSONObject jo = new JSONObject();

		List<MemuInfo> menu = menuInfoDao.getAll();

		List<Map> ret = new ArrayList<Map>();

		for (MemuInfo menuInfo : menu) {

			Map map = new HashMap();

			map.put("id", menuInfo.getId());
			map.put("pId", menuInfo.getParentId());
			map.put("name", menuInfo.getDescs());
			map.put("open", true);

			if (menuIds != null && menuIds.length() > 0 && (","+menuIds).indexOf(","+menuInfo.getId()+",") != -1) {
				map.put("checked", true);
			} else {
				map.put("checked", false);
			}

			ret.add(map);

		}

		jo.put("ret", ret);

		return "@" + jo;
	}

	@Get("createPage")
	public String createPage(Model model) {
		//MemuEnum
        model.add("MemuEnum",MemuEnum.jsgl);
		return "/views/user/roleInfoDetail.vm";
	}

	@Post("create")
	public String create(RoleInfo role, @Param("menuIds") String menuIds, HttpServletRequest request) {

		JSONObject jo = new JSONObject();

		jo.put("returnFlag", Constants.returnObj.returnFlag_true);

		int cnt = roleInfoDao.queryByName(role.getName());

		if (cnt > 0) {
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
			jo.put("code", Constants.returnObj.returnCode.code_1);
			return "@" + jo.toString();
		}

		role.setDeleteFlag(Constants.deleteFlag.deleteFlag_1);
		role.setCreateAt(DateUtils.getNowTime());
		role.setCreateUser(returnSessionObj(request).getAccount());
		role.setUpdateAt(DateUtils.getNowTime());
		role.setUpdateUser(returnSessionObj(request).getAccount());

		try {
			int roleId= roleInfoDao.create(role);
			
			createRoleMenuRef(menuIds, roleId);
			
		} catch (Exception e) {
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}

		return "@" + jo.toString();

	}

	

	@Get("edit")
	public String edit(Model model, @Param("id") int id) {
		RoleInfo role = roleInfoDao.queryById(id);

		model.add("roleInfo", role);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.jsgl);
		return "/views/user/roleInfoEdit.vm";
	}

	@Post("update")
	public String update(RoleInfo role, HttpServletRequest request) {
		JSONObject jo = new JSONObject();

		jo.put("returnFlag", Constants.returnObj.returnFlag_true);

		role.setUpdateAt(DateUtils.getNowTime());
		role.setUpdateUser(returnSessionObj(request).getAccount());

		try {
			roleInfoDao.update(role);
		} catch (Exception e) {
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}

		return "@" + jo.toString();

	}

	@Get("deleteById")
	public String deleteById(Model model, @Param("id") int id) {
		roleInfoDao.deleteById(id);
		roleMenuRefDAO.deleteByRoleId(id);
		return "r:show";
	}

	@Get("roleRefMenuEdit")
	public String roleRefMenuEdit(Model model, @Param("roleId") int roleId) {
		RoleInfo roleInfo = roleInfoDao.queryById(roleId);

		String menuIds = formMenuIdsByRoleId(roleId);

		model.add("roleInfo", roleInfo);
		model.add("menuIds", menuIds);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.jsgl);
		return "/views/user/roleRefMenuEdit.vm";
	}	

	@Post("updateRoleMenuRef")
	public String updateRoleMenuRef(RoleInfo roleInfo, @Param("menuIds")String menuIds) {
		JSONObject jo = new JSONObject();
		
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		try {
			roleMenuRefDAO.deleteByRoleId(roleInfo.getId());
			
			createRoleMenuRef(menuIds,roleInfo.getId());
		} catch (Exception e) {
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@" + jo;

	}
	
	private String formMenuIdsByRoleId(int roleId) {
		List<String> arrMenuIds = roleMenuRefDAO.getMenuIdsByRoleId(roleId);

		String menuIds = "";
		for (String menuId : arrMenuIds) {
			menuIds += menuId + ',';
		}
				
		return menuIds;
	}
	
	private void createRoleMenuRef(String menuIds, int roleId) {
		if (menuIds != null && menuIds.length() > 0) {
			for (String menuId : menuIds.split(",")) {
				RoleMenuRef roleMenuRef = new RoleMenuRef();

				roleMenuRef.setRoleId(roleId);
				roleMenuRef.setMenuId(Integer.parseInt(menuId));

				roleMenuRefDAO.create(roleMenuRef);
			}
		}
		
	}

}

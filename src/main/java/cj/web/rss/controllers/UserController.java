package cj.web.rss.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.var.Model;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cj.utils.Base64;
import cj.utils.Constants;
import cj.utils.MD5Security;
import cj.web.rss.constants.MemuEnum;
import cj.web.login.controllers.BaseController;
import cj.web.rss.dao.RoleInfoDAO;
import cj.web.rss.dao.UserInfoDAO;
import cj.web.rss.dao.UserRoleRefDAO;
import cj.web.rss.domain.RoleInfo;
import cj.web.rss.domain.UserInfo;
import cj.web.rss.domain.UserRoleRef;

@Path("/userinfo")
public class UserController extends BaseController{

	private static Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Autowired
	private UserInfoDAO userInfoDao;
	@Autowired
	private RoleInfoDAO roleInfoDao;
	@Autowired
	private UserRoleRefDAO userRoleRefDao;
	
	@Get("show")
	public String show(Model model)
	{
		//MemuEnum
        model.add("MemuEnum",MemuEnum.yhll);
		return "/views/user/userInfoList.vm";
	}

	@Post("getList")
	public String getUserList(UserInfo userInfo) {
		
		JSONObject jo = new JSONObject();
		
		List<Map> objs = userInfoDao.query(userInfo);
		
		int size = userInfoDao.querysize(userInfo);
		jo.put("data", objs);
		jo.put("draw", userInfo.getDraw());
		jo.put("recordsTotal", size);
		jo.put("recordsFiltered", size);
		return "@"+jo.toString();
	}
	
	@Get("queryById")
	public String queryById(Model model,@Param("id") int id)
	{
		userInfoDao.queryById(id);
		return null;
	}
	
	@Get("createPage")
	public String createPage(Model model)
	{
		//MemuEnum
        model.add("MemuEnum",MemuEnum.yhll);
		return "/views/user/userInfoDetail.vm";
	}
	
	@Post("create")
	public String create(Model model,UserInfo userInfo,HttpServletRequest request,@Param("roleId") String roleId)
	{
		JSONObject jo = new JSONObject();
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);

		int size = userInfoDao.queryByAccount(userInfo.getAccount());
		
		if(size>0)
		{
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
			jo.put("code", Constants.returnObj.returnCode.code_1);
			return "@"+jo.toString();
		}
		
		try {
			String pwd = userInfo.getPwd();
			userInfo.setPwd(MD5Security.md5(pwd));
			userInfo.setPhoto(Base64.getBase64(pwd));
			userInfo.setCreateUser(returnSessionObj(request).getAccount());
			userInfo.setDeleteFlag(Constants.deleteFlag.deleteFlag_1);
			userInfo.setUpdateUser(returnSessionObj(request).getAccount());
			userInfo.setType(Constants.portal.userState.state_1);
			userInfo.setState(Constants.portal.userState.state_1);
			userInfoDao.create(userInfo);
			
			if(roleId!=null && !"".equals(roleId))
				//角色ID
			{
				for(String rid:roleId.split(","))
				{
					UserRoleRef obj = new UserRoleRef();
					obj.setUserId(userInfo.getId());
					obj.setRoleId(Integer.parseInt(rid));
					userRoleRefDao.create(obj);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@"+jo.toString();
	}
	
	/**
	 * 个人编辑
	 * @param model
	 * @param request
	 * @return
	 */
	@Get("perEdit")
	public String perEdit(Model model,HttpServletRequest request)
	{
		int id = returnSessionObj(request).getId();
		UserInfo obj = userInfoDao.queryById(id);
		
		obj.setPwd(Base64.getFromBase64(obj.getPhoto()));
		
		model.add("userinfo", obj);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.yhgrxx);
		return "/views/user/perUserInfoEdit.vm";
	}
	
	/**
	 * 个人编辑方法
	 * @param model
	 * @param userInfo
	 * @param request
	 * @param roleId
	 * @return
	 */
	@Post("perUpdate")
	public String perUpdate(Model model,UserInfo userInfo,HttpServletRequest request)
	{
		JSONObject jo = new JSONObject();
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		try {
			String pwd = userInfo.getPwd();
			userInfo.setPwd(MD5Security.md5(pwd));
			userInfo.setPhoto(Base64.getBase64(pwd));
			userInfo.setUpdateUser(returnSessionObj(request).getAccount());
			userInfoDao.update(userInfo);
			
		} catch (Exception e) {
			// TODO: handle exception
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@"+jo.toString();
	}
	
	@Get("edit")
	public String edit(Model model,@Param("id") int id)
	{
		UserInfo obj = userInfoDao.queryById(id);
		
		obj.setPwd(Base64.getFromBase64(obj.getPhoto()));
		
		List<UserRoleRef> userRoleRefs = userRoleRefDao.queryId(id);
		
		String roleId = "";
		
		for(UserRoleRef urp:userRoleRefs)
		{
			roleId+=urp.getRoleId()+",";
		}
		
		model.add("roleId", roleId);
		model.add("userinfo", obj);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.yhll);
		return "/views/user/userInfoEdit.vm";
	}
	
	@Post("update")
	public String update(Model model,UserInfo userInfo,HttpServletRequest request,@Param("roleId") String roleId)
	{
		JSONObject jo = new JSONObject();
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		try {
			String pwd = userInfo.getPwd();
			userInfo.setPwd(MD5Security.md5(pwd));
			userInfo.setPhoto(Base64.getBase64(pwd));
			userInfo.setUpdateUser(returnSessionObj(request).getAccount());
			userInfoDao.update(userInfo);
			
			if(roleId!=null && !"".equals(roleId))
				//角色ID
			{
				userRoleRefDao.deleteById(userInfo.getId());
				for(String rid:roleId.split(","))
				{
					UserRoleRef obj = new UserRoleRef();
					obj.setUserId(userInfo.getId());
					obj.setRoleId(Integer.parseInt(rid));
					userRoleRefDao.create(obj);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@"+jo.toString();
	}
	
	@Get("deleteById")
	public String deleteById(Model model,@Param("id") int id)
	{
		userInfoDao.deleteById(id);
		return "r:show";
	}
	
	@Get("userRefRoleJson")
	public String userRefRoleJson(@Param("roleId") String roleId)
	{
		JSONObject jo = new JSONObject();
		List<RoleInfo> roleInfos = roleInfoDao.getListAll();
		List<Map> reps = new ArrayList<Map>();
		for(RoleInfo obj:roleInfos)
		{
			Map rep = new HashMap();
			rep.put("id", obj.getId());
			rep.put("pId", 0);
			rep.put("name", obj.getName());
			rep.put("open", true);
			if(roleId!=null)
			{
				rep.put("checked", (","+roleId).indexOf(","+obj.getId()+",")!=-1?true:false);
			}
			else
			{
				rep.put("checked", false);
			}
			
			reps.add(rep);
		}
		jo.put("reps", reps);
		return "@"+jo.toString();
	}
}

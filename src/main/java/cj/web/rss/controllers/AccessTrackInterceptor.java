package cj.web.rss.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;

import net.paoding.rose.web.ControllerInterceptorAdapter;
import net.paoding.rose.web.Invocation;
import cj.utils.MD5Security;
import cj.web.admin.constants.AccessTrackEnum;

/**
 * 拦截器
 * @author Administrator
 *
 */
public class AccessTrackInterceptor extends ControllerInterceptorAdapter {
	
//	@Autowired
//	private MsgLogDAO msgLogDAO;
	
    public AccessTrackInterceptor() {
        setPriority(29600);
    }

//    @Override
//    public Class<? extends Annotation> getRequiredAnnotationClass() {
//    	System.out.println("1111");
//        return PriCheckRequired.class; // 这是一个注解，只有标过的controller才会接受这个拦截器的洗礼。
//    }

    @Override
    public Object before(Invocation inv) throws Exception {
    	//---------------cookie中获取参数，转换成Map
    	Map<String,Cookie> cookieMap = new HashMap<String,Cookie>();
    	Cookie[] cookies = inv.getRequest().getCookies();
        if(null!=cookies){
            for(Cookie cookie : cookies){
                cookieMap.put(cookie.getName(), cookie);
            }
        }
    	//---------------查看cookie中有没有索要字段
        if(cookieMap.containsKey("account")){
        	//帐号
        	String account = cookieMap.get("account").getValue();
        	//cookie中的Key
        	String _token_ = cookieMap.get("_token_").getValue();
        	//取出加密后的Key
        	String token = MD5Security.returnSingKey(account);
        	
        	if(!_token_.equals(token))
            {
        		return Boolean.FALSE;
            }
        	
        	//存在
        	boolean flag = AccessTrackEnum.reMethodExist(inv.getMethod().getName());
        	
//        	if(flag)
//        	{
//        		MsgLog obj = new MsgLog();
//            	obj.setCreateUser(cookieMap.get("account").getValue());
//            	obj.setInvocation(inv.getModel().get("invocation").toString());
//            	obj.setMsg(inv.getModel().getAttributes().toString());
//            	
//            	msgLogDAO.create(obj);
//        	}
        	
            return Boolean.TRUE;
        }else{
        	//不存在
            return Boolean.FALSE;
        } 
    }
    
    @Override
    public void afterCompletion(final Invocation inv, Throwable ex) throws Exception {
//    	System.out.println("-----");
    }
}
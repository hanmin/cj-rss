package cj.web.rss.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.var.Model;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cj.utils.Constants;
import cj.web.login.controllers.BaseController;
import cj.web.rss.constants.MemuEnum;
import cj.web.rss.dao.ConfigInfoDAO;
import cj.web.rss.domain.ConfigInfo;

@Path("/configinfo")
public class ConfigInfoController extends BaseController{

	private static Logger logger = Logger.getLogger(ConfigInfoController.class.getName());
	
	@Autowired
	private ConfigInfoDAO configInfoDao;

	@Get("show")
	public String show(Model model,ConfigInfo configInfo)
	{
		//MemuEnum
        model.add("MemuEnum",MemuEnum.pzgl);
		return "/views/config/configInfoList.vm";
	}

	@Post("getList")
	public String getUserList(ConfigInfo configInfo) {
		
		JSONObject jo = new JSONObject();
		
		List<ConfigInfo> objs = configInfoDao.query(configInfo);
		
		int size = configInfoDao.querysize(configInfo);
		jo.put("data", objs);
		jo.put("draw", configInfo.getDraw());
		jo.put("recordsTotal", size);
		jo.put("recordsFiltered", size);
		return "@"+jo.toString();
	}
	
	@Get("queryById")
	public String queryById(Model model,@Param("id") int id)
	{
		configInfoDao.queryById(id);
		return null;
	}
	
	@Get("createPage")
	public String createPage(Model model,ConfigInfo configInfo)
	{
		model.add("configInfo", configInfo);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.pzgl);
		return "/views/config/configInfoDetail.vm";
	}
	
	@Post("create")
	public String create(Model model,ConfigInfo configInfo,HttpServletRequest request)
	{
		JSONObject jo = new JSONObject();
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		if(configInfo.getParentId()==null || "".equals(configInfo.getParentId()))
		{
			configInfo.setParentId("0");
		}
		
		try {
			configInfo.setCreateUser(returnSessionObj(request).getAccount());
			configInfo.setDeleteFlag(Constants.deleteFlag.deleteFlag_1);
			configInfo.setUpdateUser(returnSessionObj(request).getAccount());
			configInfoDao.create(configInfo);
		} catch (Exception e) {
			// TODO: handle exception
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@"+jo.toString();
	}
	
	@Get("edit")
	public String edit(Model model,@Param("id") int id)
	{
		ConfigInfo obj = configInfoDao.queryById(id);
		model.add("configInfo", obj);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.pzgl);
		return "/views/config/configInfoEdit.vm";
	}
	
	@Post("update")
	public String update(Model model,ConfigInfo configInfo,HttpServletRequest request)
	{
		JSONObject jo = new JSONObject();
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		try {
			configInfo.setUpdateUser(returnSessionObj(request).getAccount());
			configInfoDao.update(configInfo);
		} catch (Exception e) {
			// TODO: handle exception
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@"+jo.toString();
	}
	
	@Get("deleteById")
	public String deleteById(Model model,@Param("id") int id)
	{
		configInfoDao.deleteById(id);
		return "r:show";
	}
}

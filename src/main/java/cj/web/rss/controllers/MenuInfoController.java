package cj.web.rss.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.var.Model;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;

import cj.utils.Constants;
import cj.utils.DateUtils;
import cj.web.rss.constants.MemuEnum;
import cj.web.login.controllers.BaseController;
import cj.web.rss.dao.MemuInfoDAO;
import cj.web.rss.domain.MemuInfo;

@Path("menuinfo")
public class MenuInfoController extends BaseController {
	@Autowired
	private MemuInfoDAO menuInfoDao;
	
	@Get("show")
	public String showMenuInfo(Model model){
		//MemuEnum
        model.add("MemuEnum",MemuEnum.cdgl);
		return "/views/menu/menuInfoList.vm";
	}
	
	@Post("getList")
    public String getList(MemuInfo menu){
		List<MemuInfo> arr = menuInfoDao.getList(menu);
		
		JSONObject jo = new JSONObject();
		
		int size = menuInfoDao.getListSize(menu);
		
		jo.put("data", arr);
		jo.put("draw", menu.getDraw());
		jo.put("recordsTotal", size);
		jo.put("recordsFiltered", size);
		
		return "@" + jo.toString();
	}
	
	@Get("createPage")
	public String createPage(Model model,@Param("parentId") String parentId){
		//MemuEnum
        model.add("MemuEnum",MemuEnum.cdgl);
		return "/views/menu/menuInfoDetail.vm";
	}
	
	@Post("create")
	public String createMenu(MemuInfo menu,HttpServletRequest request){
		
		JSONObject jo = new JSONObject();
		
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		menu.setDeleteFlag(Constants.deleteFlag.deleteFlag_1);
		menu.setCreateAt(DateUtils.getNowTime());
		menu.setCreateUser(returnSessionObj(request).getAccount());
		menu.setUpdateAt(DateUtils.getNowTime());
		menu.setUpdateUser(returnSessionObj(request).getAccount());
		
		try{
			menuInfoDao.create(menu);
		}
		catch(Exception e){
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@" + jo.toString();
		
	}
		
	@Get("deleteById")
	public String deleteById(@Param("id") int id)
	{
		menuInfoDao.deleteById(id);
		return "r:show";
	}
	
	@Get("edit")
	public String editById(Model model,@Param("id") int id){
		MemuInfo menu = menuInfoDao.queryById(id);
		
		model.add("menuInfo", menu);
		//MemuEnum
        model.add("MemuEnum",MemuEnum.cdgl);
		return "/views/menu/menuInfoEdit.vm";
	}
	
	@Post("update")
	public String updateById(MemuInfo menu,HttpServletRequest request){
		JSONObject jo = new JSONObject();
		
		jo.put("returnFlag", Constants.returnObj.returnFlag_true);
		
		menu.setUpdateAt(DateUtils.getNowTime());
		menu.setUpdateUser(returnSessionObj(request).getAccount());
		
		try {
			menuInfoDao.update(menu);
		} catch (Exception e) {
			jo.put("returnFlag", Constants.returnObj.returnFlag_flase);
		}
		
		return "@" + jo.toString();
		
	}

}

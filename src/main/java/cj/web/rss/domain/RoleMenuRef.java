package cj.web.rss.domain;
/*
 *  
 *  
*/
import java.io.Serializable;

/**
 * TODO: add class/table comments
 */
public class RoleMenuRef implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7505562348808443266L;
	protected int id;
	protected int roleId;
	protected int menuId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + menuId;
		result = prime * result + roleId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleMenuRef other = (RoleMenuRef) obj;
		if (id != other.id)
			return false;
		if (menuId != other.menuId)
			return false;
		if (roleId != other.roleId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RoleMenuRef [id=" + id + ", roleId=" + roleId + ", menuId="
				+ menuId + "]";
	}
}

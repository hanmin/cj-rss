package cj.web.rss.domain;
/*
 *  
 *  
*/
import java.io.Serializable;

/**
 * TODO: add class/table comments
 */
public class UserInfo extends DataTablePage implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1568867718960577938L;
	protected int id;
	protected String photo;
	protected String account;
	protected String name;
	protected String descs;
	protected String pwd;
	protected String phone;
	protected String email;
	protected Integer type;
	protected Integer state;
	protected Integer deleteFlag;
	protected String createUser;
	protected String createAt;
	protected String updateUser;
	protected String updateAt;
	protected String devicesId;
	
	protected String weixinuuid;
	protected String qquuid;
	protected String weibouuid;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescs() {
		return descs;
	}
	public void setDescs(String descs) {
		this.descs = descs;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateAt() {
		return createAt;
	}
	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(String updateAt) {
		this.updateAt = updateAt;
	}
	public String getDevicesId() {
		return devicesId;
	}
	public void setDevicesId(String devicesId) {
		this.devicesId = devicesId;
	}
	public String getWeixinuuid() {
		return weixinuuid;
	}
	public void setWeixinuuid(String weixinuuid) {
		this.weixinuuid = weixinuuid;
	}
	public String getQquuid() {
		return qquuid;
	}
	public void setQquuid(String qquuid) {
		this.qquuid = qquuid;
	}
	public String getWeibouuid() {
		return weibouuid;
	}
	public void setWeibouuid(String weibouuid) {
		this.weibouuid = weibouuid;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result
				+ ((createAt == null) ? 0 : createAt.hashCode());
		result = prime * result
				+ ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result
				+ ((deleteFlag == null) ? 0 : deleteFlag.hashCode());
		result = prime * result + ((descs == null) ? 0 : descs.hashCode());
		result = prime * result
				+ ((devicesId == null) ? 0 : devicesId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		result = prime * result + ((pwd == null) ? 0 : pwd.hashCode());
		result = prime * result + ((qquuid == null) ? 0 : qquuid.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((updateAt == null) ? 0 : updateAt.hashCode());
		result = prime * result
				+ ((updateUser == null) ? 0 : updateUser.hashCode());
		result = prime * result
				+ ((weibouuid == null) ? 0 : weibouuid.hashCode());
		result = prime * result
				+ ((weixinuuid == null) ? 0 : weixinuuid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (createAt == null) {
			if (other.createAt != null)
				return false;
		} else if (!createAt.equals(other.createAt))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (deleteFlag == null) {
			if (other.deleteFlag != null)
				return false;
		} else if (!deleteFlag.equals(other.deleteFlag))
			return false;
		if (descs == null) {
			if (other.descs != null)
				return false;
		} else if (!descs.equals(other.descs))
			return false;
		if (devicesId == null) {
			if (other.devicesId != null)
				return false;
		} else if (!devicesId.equals(other.devicesId))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		if (pwd == null) {
			if (other.pwd != null)
				return false;
		} else if (!pwd.equals(other.pwd))
			return false;
		if (qquuid == null) {
			if (other.qquuid != null)
				return false;
		} else if (!qquuid.equals(other.qquuid))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (updateAt == null) {
			if (other.updateAt != null)
				return false;
		} else if (!updateAt.equals(other.updateAt))
			return false;
		if (updateUser == null) {
			if (other.updateUser != null)
				return false;
		} else if (!updateUser.equals(other.updateUser))
			return false;
		if (weibouuid == null) {
			if (other.weibouuid != null)
				return false;
		} else if (!weibouuid.equals(other.weibouuid))
			return false;
		if (weixinuuid == null) {
			if (other.weixinuuid != null)
				return false;
		} else if (!weixinuuid.equals(other.weixinuuid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserInfo [id=").append(id).append(", photo=")
				.append(photo).append(", account=").append(account)
				.append(", name=").append(name).append(", descs=")
				.append(descs).append(", pwd=").append(pwd).append(", phone=")
				.append(phone).append(", email=").append(email)
				.append(", type=").append(type).append(", state=")
				.append(state).append(", deleteFlag=").append(deleteFlag)
				.append(", createUser=").append(createUser)
				.append(", createAt=").append(createAt).append(", updateUser=")
				.append(updateUser).append(", updateAt=").append(updateAt)
				.append(", devicesId=").append(devicesId)
				.append(", weixinuuid=").append(weixinuuid).append(", qquuid=")
				.append(qquuid).append(", weibouuid=").append(weibouuid)
				.append("]");
		return builder.toString();
	}
}
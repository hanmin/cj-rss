package cj.web.rss.domain;

import java.io.Serializable;

public class DataTablePage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -453238201820387626L;
	
	public int draw;
	public int start;
	public int length;
	
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + draw;
		result = prime * result + length;
		result = prime * result + start;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataTablePage other = (DataTablePage) obj;
		if (draw != other.draw)
			return false;
		if (length != other.length)
			return false;
		if (start != other.start)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DataTablePage [draw=" + draw + ", start=" + start + ", length="
				+ length + "]";
	}
}

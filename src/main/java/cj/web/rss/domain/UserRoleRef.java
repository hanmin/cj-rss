package cj.web.rss.domain;
/*
 *  
 *  
*/
import java.io.Serializable;

/**
 * TODO: add class/table comments
 */
public class UserRoleRef implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5322198640210813430L;
	protected int id;
	protected int roleId;
	protected int userId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + roleId;
		result = prime * result + userId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRoleRef other = (UserRoleRef) obj;
		if (id != other.id)
			return false;
		if (roleId != other.roleId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UserRoleRef [id=" + id + ", roleId=" + roleId + ", userId="
				+ userId + "]";
	}
}

package cj.web.rss.util;

import java.util.List;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cj.utils.DateUtils;
import cj.web.rss.constants.EvacuateEnum;
import cj.web.rss.dao.RssNewsDAO;
import cj.web.rss.domain.RssNews;

/**
 * QuartzTask
 * @author hanmin
 *
 */
public class LzstoneQuartzTask {
	
	private static Logger logger = Logger.getLogger(LzstoneQuartzTask.class.getName());

	@Autowired
	public RssNewsDAO rssNewsDao;
	
	/**
	 * 初始化新华社订阅新闻
	 */
	public void executeXHSRssNews() {
		logger.info("======executeXHSRssNews======"+DateUtils.getNowTime());
		
		String responseRss = RssUtil.invokeRss();
		try {
			JSONObject jsonObject = JSONObject.fromObject(responseRss);
			logger.info("======return is error======"+responseRss);
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("======return is OK======");
		}
		
		List<RssNews> rssNewslis = RssUtil.analysisRssItem(responseRss);
		
		for(RssNews obj:rssNewslis)
		{
			String link = obj.getLink();//链接地址唯一值
			String updateDate = obj.getUpdateDate();//最后更新时间
			
			//正常
			try {
				int rssNewsSize = rssNewsDao.queryByLinkSize(link,updateDate);
				
				if(rssNewsSize==0)
				{
					rssNewsDao.create(obj);
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
}
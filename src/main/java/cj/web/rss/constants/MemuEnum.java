package cj.web.rss.constants;

/**
 * 菜单枚举
 * @author Administrator
 *
 */
public enum MemuEnum {
	//Rss新闻
	rssnews("RSS_新闻", "rss_news"),
	rssrecord("RSS_对稿", "rss_record"),
	//用户管理
	yhgrxx("后台用户管理_个人信息", "yhgl_yhgrxx"),
	yhll("后台用户管理_用户列表", "yhgl_yhll"),
	cdgl("后台用户管理_菜单管理", "yhgl_cdgl"),
	jsgl("后台用户管理_角色管理", "yhgl_jsgl"),
	
	//配置管理
	pzgl("配置管理_配置", "pzgl_pz"),
	;
	
    // 成员变量
    private String name;
    private String value;

    // 构造方法
    private MemuEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(String value) {
        for (MemuEnum c : MemuEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

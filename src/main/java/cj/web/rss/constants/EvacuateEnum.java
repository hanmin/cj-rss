package cj.web.rss.constants;

/**
 * 订阅稿件状态
 * @author Administrator
 *
 */
public enum EvacuateEnum {
	//Rss新闻状态
	EVACUATE_0("正常", 0),
	EVACUATE_2("改稿", 2),
	EVACUATE_3("撤稿", 3),
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private EvacuateEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (EvacuateEnum c : EvacuateEnum.values()) {
            if (c.getValue()==value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}

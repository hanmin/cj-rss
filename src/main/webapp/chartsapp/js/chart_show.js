$(function(){
	(function(){
		//微信
		var myChart = echarts.init(document.getElementById('chartDemo'));
	    myChart.setOption({
	    	title : {
	            text: '统计报表',
	            subtext: '用户关注数'
	        },
	        tooltip : {
	            trigger: 'axis'
	        },
	        legend: {
	            data:['用户数','新增用户数','取消用户数'],
	            x: 'right'
	        },
	        calculable : true,
	        xAxis : [
	            {
	                type : 'category',
	                data : (function() {
	                    var arr = [];
	                    $.ajax({
	                        type: "get",
	                        async : false,
	                        url: "weixinuserreport/getusersummary?begin_date=2015-07-28&end_date=2015-08-02",
	                        dataType: "json",
	                        success: function(data) {
	                            if (data.returnFlag) {
	                            	var list = data.list;
	                            	var len = list.length;
	                                for (var i=0; i<len; i++) {
	                                    arr.push(list[i].ref_date);
	                                }
	                            }
	                        },
	                        error: function(errorMsg) {
	                            alert("请求数据失败!");
	                            myChart.hideLoading();
	                        }
	                    })
	                    return arr;
	                }())
	            }
	        ],
	        yAxis : [
	            {
	                type : 'value',
	                splitArea : {show : true}
	            }
	        ],
	        series : [
	            {
	                name:'用户数',
	                type:'bar',
	                data: (function() {
	                    var arr = [];
	                    $.ajax({
	                        type: "get",
	                        async : false,
	                        url: "weixinuserreport/getusersummary?begin_date=2015-07-28&end_date=2015-08-02",
	                        dataType: "json",
	                        success: function(data) {
	                            if (data.returnFlag) {
	                            	var list = data.list;
	                            	var len = list.length;
	                                for (var i=0; i<len; i++) {
	                                    arr.push(list[i].user_source);
	                                }
	                            }
	                        },
	                        error: function(errorMsg) {
	                            alert("请求数据失败!");
	                            myChart.hideLoading();
	                        }
	                    })
	                    return arr;
	                }())
	            },
	            {
	                name:'新增用户数',
	                type:'bar',
	                data: (function() {
	                    var arr = [];
	                    $.ajax({
	                        type: "get",
	                        async : false,
	                        url: "weixinuserreport/getusersummary?begin_date=2015-07-28&end_date=2015-08-02",
	                        dataType: "json",
	                        success: function(data) {
	                            if (data.returnFlag) {
	                            	var list = data.list;
	                            	var len = list.length;
	                                for (var i=0; i<len; i++) {
	                                    arr.push(list[i].new_user);
	                                }
	                            }
	                        },
	                        error: function(errorMsg) {
	                            alert("请求数据失败!");
	                            myChart.hideLoading();
	                        }
	                    })
	                    return arr;
	                }())
	            },
	            {
	                name:'取消用户数',
	                type:'bar',
	                data: (function() {
	                    var arr = [];
	                    $.ajax({
	                        type: "get",
	                        async : false,
	                        url: "weixinuserreport/getusersummary?begin_date=2015-07-28&end_date=2015-08-02",
	                        dataType: "json",
	                        success: function(data) {
	                            if (data.returnFlag) {
	                            	var list = data.list;
	                            	var len = list.length;
	                                for (var i=0; i<len; i++) {
	                                    arr.push(list[i].cancel_user);
	                                }
	                            }
	                        },
	                        error: function(errorMsg) {
	                            alert("请求数据失败!");
	                            myChart.hideLoading();
	                        }
	                    })
	                    return arr;
	                }())
	            }
	        ]
	    });
	    
	    //腾讯微博
	    
	    //新浪微博

	}());
});
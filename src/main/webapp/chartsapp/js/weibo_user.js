$(function(){
    //初始化显示前一天及之前七天
    (function(){
    	var date = new Date(new Date().getTime() - 24*1*60*60*1000);//前一天的日期
		var end_date = date.format("yyyy-MM-dd");
		var date2 = new Date(date.getTime() - 24*6*60*60*1000);//前一天之前的第七天的日期
		var begin_date = date2.format("yyyy-MM-dd");
         
        $("#date_plugin").val(begin_date + " - " +end_date);

        $.ajax({
            type: "get",
            url: 'weiboreport/getweibouser?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                if (data.returnFlag) {
                    var objs = data.objs;

                    //处理数据
                    $.each(objs, function(i, value){
                        value.refDate = date_substr(value.refDate);//日期
                    });
                    
                    var dateArr = [],
                        net_arr = [],
                        total_arr = [];

                    for (var i = 0; i < objs.length; i++) {
                        dateArr.push(objs[i].refDate);//日期
                        net_arr.push(objs[i].weibo_net_user);//净增
                        total_arr.push(objs[i].weibo_total_user);//累计
                    }

                    //画折线
                    DrawLineEChart("net_add", "净增", dateArr, net_arr);
                    DrawLineEChart("stat_total", "累计", dateArr, total_arr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
                myChart.hideLoading();
            }
       });
    }());
    
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "131px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $.ajax({
                 type: "get",
                 url: 'weiboreport/getweibouser?begin_date='+begin_date+'&end_date='+end_date,
                 dataType: "json",
                 success: function(data) {
                    var data = data;
                    if (data.returnFlag) {
                        var objs = data.objs;

                        //处理数据
                        $.each(objs, function(i, value){
                            value.refDate = date_substr(value.refDate);//日期
                        });

                        var dateArr = [],
                            net_arr = [],
                            total_arr = [];

                        for (var i = 0; i < objs.length; i++) {
                            dateArr.push(objs[i].refDate);//日期
                            net_arr.push(objs[i].weibo_net_user);//净增
                            total_arr.push(objs[i].weibo_total_user);//累计
                        }

                        //画折线
                        DrawLineEChart("net_add", "净增", dateArr, net_arr);
                        DrawLineEChart("stat_total", "累计", dateArr, total_arr);
                     }
                 },
                 error: function(errorMsg) {
                     alert("请求数据失败!");
                     myChart.hideLoading();
                 }
            });
        });
    }());
});

//图表函数
function DrawLineEChart(id, title, dateArr, dataArr){
    var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis'
        },
        calculable : true,
        grid: {
            x: 50,
            y: 10,
            x2: 18,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title,
                type:'line',
                stack: '总量',
                data: dataArr
            }
        ]
    };
                    
    myChart.setOption(option);
};
$(function(){
    //初始化显示当天的数据
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var begin_date = end_date;
        
        $("#date_plugin").val(end_date);

        $.ajax({
            type: "get",
            url: 'appreport/getappcloumncount?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if(data.returnFlag) {
                    var columnls = data.columnls;

                    for (var i = 0; i < columnls.length; i++) {

                        //动态添加栏目标题
                        var htmlTitle = "";
                        htmlTitle += "<div class='container pad-b10' style='margin-left: 20px;margin-right: 20px;'>" ;
                        htmlTitle += "<div class='column-title clearfix'>";
                        htmlTitle += "<div class='col-xs-10 text-center' style='padding-right: 0;'>";
                        htmlTitle += "<div class='column-title-txt'>" + columnls[i].columnName+ "</div>";
                        htmlTitle += "</div>";
                        htmlTitle += "<div class='col-xs-2' style='padding-right: 0;padding-left: 0;'>";
                        htmlTitle += "<a class='column-title-btn' href='app_subColumn?columnId="+columnls[i].columnId+"'><span class='glyphicon glyphicon-menu-right'></span></a>";
                        htmlTitle += "</div>";
                        htmlTitle += "</div>";
                        htmlTitle += "</div>";

                        //动态添加菜单数据
                        var repls = columnls[i].repls;
                        var htmlList = "", htmlTemp = $("textarea").val();
                        for(var j = 0;j < repls.length; j++){
                            var htmlCont = "";
                            repls.forEach(function(object) {
                                htmlCont += htmlTemp.temp(object);
                            });
                        }
                        //console.log(htmlCont);
                        
                        //创建栏目菜单
                        htmlList += "<ul class='target-menu'>"; 
                        htmlList += "<li>栏目</li>";
                        htmlList += "<li>发稿数</li>";
                        htmlList += "<li>阅读</li>";
                        htmlList += "<li>转发</li>";
                        htmlList += "<li>评论</li>";
                        htmlList += "<li>点赞</li>";
                        htmlList += "<li>传播活性</li>";
                        htmlList += "</ul>";

                        //创建栏目菜单数据容器
                        htmlList += "<div class='table-cont column_item'>";
                        htmlList += htmlCont;
                        htmlList += "</div>";

                        var htmlStr = htmlTitle + htmlList;
                        //console.log(htmlStr);
                        $("<div class='mar-t30 lanmu-cont'>").html(htmlStr).appendTo($("#column_cont"));//插入到APP栏目容器
                    }
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
        });
    }());

    //日期控件选择日期
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "78px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
       $('#date_plugin').daterangepicker({
            singleDatePicker: true
       }, function(end){
            var end_date = new Date(end).format("yyyy-MM-dd");
            var begin_date = end_date;
            
            $(".lanmu-cont").remove();//清空数据
            
            $.ajax({
                type: "get",
                url: 'appreport/getappcloumncount?begin_date='+begin_date+'&end_date='+end_date,
                dataType: "json",
                success: function(data) {
                    var data = data;
                    
                    if(data.returnFlag) {
                        var columnls = data.columnls;

                        for (var i = 0; i < columnls.length; i++) {

                            //动态添加栏目标题
                            var htmlTitle = "";
                            htmlTitle += "<div class='container pad-b10' style='margin-left: 20px;margin-right: 20px;'>" ;
                            htmlTitle += "<div class='column-title clearfix'>";
                            htmlTitle += "<div class='col-xs-10 text-center' style='padding-right: 0;'>";
                            htmlTitle += "<div class='column-title-txt'>" + columnls[i].columnName+ "</div>";
                            htmlTitle += "</div>";
                            htmlTitle += "<div class='col-xs-2' style='padding-right: 0;padding-left: 0;'>";
                            htmlTitle += "<a class='column-title-btn' href='app_subColumn?columnId="+columnls[i].columnId+"' data-column='"+columnls[i].columnId+"'><span class='glyphicon glyphicon-menu-right'></span></a>";
                            htmlTitle += "</div>";
                            htmlTitle += "</div>";
                            htmlTitle += "</div>";

                            //动态添加菜单数据
                            var repls = columnls[i].repls;
                            var htmlList = "", htmlTemp = $("textarea").val();
                            for(var j = 0;j < repls.length; j++){
                                var htmlCont = "";
                                repls.forEach(function(object) {
                                    htmlCont += htmlTemp.temp(object);
                                });
                            }
                            //console.log(htmlCont);
                            
                            //创建栏目菜单
                            htmlList += "<ul class='target-menu'>"; 
                            htmlList += "<li>栏目</li>";
                            htmlList += "<li>发稿数</li>";
                            htmlList += "<li>阅读</li>";
                            htmlList += "<li>转发</li>";
                            htmlList += "<li>评论</li>";
                            htmlList += "<li>点赞</li>";
                            htmlList += "<li>传播活性</li>";
                            htmlList += "</ul>";

                            //创建栏目菜单数据容器
                            htmlList += "<div class='table-cont column_item'>";
                            htmlList += htmlCont;
                            htmlList += "</div>";

                            var htmlStr = htmlTitle + htmlList;
                            //console.log(htmlStr);
                            $("<div class='mar-t30 lanmu-cont'>").html(htmlStr).appendTo($("#column_cont"));//插入到博文容器
                        }
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
});
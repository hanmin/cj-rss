$(function(){
	//初始化显示前一天及之前七天的数据
	(function(){
		var date = new Date(new Date().getTime() - 24*1*60*60*1000);//前一天的日期
		var end_date = date.format("yyyy-MM-dd");
		var date2 = new Date(date.getTime() - 24*6*60*60*1000);//前一天之前的第七天的日期
		var begin_date = date2.format("yyyy-MM-dd");
		
		$("#date_plugin").val(begin_date + " - " +end_date);
		$.ajax({
            type: "get",
            url: 'weixinuserreport/getusersummary?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
            	var data = data;
            	if (data.returnFlag) {
                    var returnls = data.returnls;
                    
                    //处理数据
                    $.each(returnls, function(i, value){
                        value.ref_date = date_substr(value.ref_date);//日期
                        value.new_user_rate = rate_judge(value.new_user_rate);//新增率
                        value.net_user_rate = rate_judge(value.net_user_rate);//净增率
                        value.total_user_rate = rate_judge(value.total_user_rate);//累计率
                    });

                    var dateArr = [], 
                        new_arr = [], 
                        net_arr = [],
                        total_arr = [],
                        newRate_arr = [],
                        netRate_arr = [],
                        totalRate_arr = [];

                    for (var i = 0; i < returnls.length; i++) {
                        dateArr.push(returnls[i].ref_date);//日期
                        new_arr.push(returnls[i].new_user);//新增
                        net_arr.push(returnls[i].net_user);//净增
                        total_arr.push(returnls[i].total_user);//累计
                        newRate_arr.push(returnls[i].new_user_rate);//新增率
                        netRate_arr.push(returnls[i].net_user_rate);//净增率
                        totalRate_arr.push(returnls[i].total_user_rate);//累计率
                    }

                    //画折线
                    DrawLineEChart("new_add", "新增", dateArr, new_arr, newRate_arr);
                    DrawLineEChart("net_add", "净增", dateArr, net_arr, netRate_arr);
                    DrawLineEChart("stat_total", "累计", dateArr, total_arr, totalRate_arr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
                myChart.hideLoading();
            }
       });
	}());
    
	(function(){
		//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "131px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
		//日期控件选择日期
		$('#date_plugin').daterangepicker(null, function(start, end) {
			var begin_date = new Date(start).format("yyyy-MM-dd");
	        var end_date = new Date(end).format("yyyy-MM-dd");

	        $.ajax({
	             type: "get",
	             url: 'weixinuserreport/getusersummary?begin_date='+begin_date+'&end_date='+end_date,
	             dataType: "json",
	             success: function(data) {
	             	var data = data;
	             	if (data.returnFlag) {
	                    var returnls = data.returnls;
	                    
	                    //处理数据
	                    $.each(returnls, function(i, value){
	                        value.ref_date = date_substr(value.ref_date);//日期
	                        value.new_user_rate = rate_judge(value.new_user_rate);//新增率
	                        value.net_user_rate = rate_judge(value.net_user_rate);//净增率
	                        value.total_user_rate = rate_judge(value.total_user_rate);//累计率
	                    });

	                    var dateArr = [],
	                        new_arr = [],
	                        net_arr = [],
	                        total_arr = [],
	                        newRate_arr = [],
	                        netRate_arr = [],
	                        totalRate_arr = [];

	                    for (var i = 0; i < returnls.length; i++) {
	                    	dateArr.push(returnls[i].ref_date);//日期
	                        new_arr.push(returnls[i].new_user);//新增
	                        net_arr.push(returnls[i].net_user);//净增
	                        total_arr.push(returnls[i].total_user);//累计
	                        newRate_arr.push(returnls[i].new_user_rate);//新增率
	                        netRate_arr.push(returnls[i].net_user_rate);//净增率
	                        totalRate_arr.push(returnls[i].total_user_rate);//累计率
	                    }

	                    //画折线
	                    DrawLineEChart("new_add", "新增", dateArr, new_arr, newRate_arr);
	                    DrawLineEChart("net_add", "净增", dateArr, net_arr, netRate_arr);
	                    DrawLineEChart("stat_total", "累计", dateArr, total_arr, totalRate_arr)
	             	}
	             },
	             error: function(errorMsg) {
	                 alert("请求数据失败!");
                     myChart.hideLoading();
	             }
	        });
	    });
	}());
});

//图表函数
function DrawLineEChart(id, title, dateArr, dataArr, rateArr){
	var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis',
            formatter: function (params,ticket,callback) {
                //console.log(ticket);
                var result = "";
                var res = params[0].name + '<br/>' + params[0].seriesName + ' : ' + params[0].value;
                var re = '<br/>' + '增长率' + ' : ' + rateArr[ticket.split(":")[1]];
                result = res + re;
                setTimeout(function (){
                    // 仅为了模拟异步回调
                    callback(ticket, result);
                }, 0)
            }
        },
        calculable : true,
        grid: {
            x: 50,
            y: 10,
            x2: 20,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title,
                type:'line',
                stack: '总量',
                data: dataArr
            }
        ]
    };
                    
    myChart.setOption(option);
};
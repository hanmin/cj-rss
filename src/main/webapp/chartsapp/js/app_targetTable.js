$(function(){
    //初始化显示当天及过去七天的数据
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " +end_date);

        $.ajax({
            type: "get",
            url: 'appreport/getappvisitcount?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                if (data.returnFlag) {
                    var reps = data.reps;
                    
                    //处理数据
                    $.each(reps, function(i, value){
                        value.redDate = date_substr(value.redDate);//日期
                        value.app_download_rate = rate_judge(value.app_download_rate);//下载率
                        value.app_register_rate = rate_judge(value.app_register_rate);//注册率
                        value.app_install_rate = rate_judge(value.app_install_rate);//安装率
                        value.app_active_rate = rate_judge(value.app_active_rate);//日活率
                        value.app_unload_rate = rate_judge(value.app_unload_rate);//卸载率
                    });

                    //动态添加DOM
                    var htmlList = "", htmlTemp = $("textarea").val();
                    reps.forEach(function(object) {
                        htmlList += htmlTemp.temp(object);
                    });
                    //console.log(htmlList);
                    $("#target_cont").html(htmlList);//插入到指标内容容器
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "121px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $(".table-item").remove();//清空数据

            $.ajax({
                 type: "get",
                 url: 'appreport/getappvisitcount?begin_date='+begin_date+'&end_date='+end_date,
                 dataType: "json",
                 success: function(data) {
                    var data = data;
                    
                    if (data.returnFlag) {
                        var reps = data.reps;
                        
                        //处理数据
                        $.each(reps, function(i, value){
                            value.redDate = date_substr(value.redDate);//日期
                            value.app_download_rate = rate_judge(value.app_download_rate);//下载率
                            value.app_register_rate = rate_judge(value.app_register_rate);//注册率
                            value.app_install_rate = rate_judge(value.app_install_rate);//安装率
                            value.app_active_rate = rate_judge(value.app_active_rate);//日活率
                            value.app_unload_rate = rate_judge(value.app_unload_rate);//卸载率
                        });

                        //动态添加DOM
                        var htmlList = "", htmlTemp = $("textarea").val();
                        reps.forEach(function(object) {
                            htmlList += htmlTemp.temp(object);
                        });
                        //console.log(htmlList);
                        $("#target_cont").html(htmlList);//插入到指标内容容器
                    }
                 },
                 error: function(errorMsg) {
                     alert("请求数据失败!");
                     myChart.hideLoading();
                 }
            });
        });
    }());
});
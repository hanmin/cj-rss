$(function(){
    (function(){
        $.ajax({
            type: "get",
            url: "appreport/getappchanneldatahome",
            dataType: "json",
            success: function(data) {
                var data = data;
                if (data.returnFlag) {
                    var data = data;
                    
                    //关键指标概述
                    $.each(data.appkeyIndicatorsMap, function(name, value){
                        $(".js_zhibiao").each(function(){
                            if(name === this.id){
                                $(this).html(value);
                            }
                        });
                    });
                    
                    //栏目实时数据
                    $.each(data.appVisitManuscriptMap, function(name, value){
                        $(".js_gaojian").each(function(){
                            if(name === this.id){
                                $(this).html(value);
                            }
                        });
                    });
                    
                    //稿件实时数据
                    var columnData = data.appVisitColumnls;
                    var htmlStr = "";
                    for (var i = 0; i < columnData.length; i++) {
                        htmlStr += "<ul class='channel-menu-item'>";
                        htmlStr += "<li>" + columnData[i].columnName + "</li>" 
                                          + "<li>" + columnData[i].readCount + "</li>"
                                          + "<li>" + columnData[i].forwardingNo + "</li>"
                                          + "<li>" + columnData[i].commentNo + "</li>"
                                          + "<li>" + columnData[i].pointNo + "</li>";
                        htmlStr += "</ul>";
                    }
                    $("#channel_cont").append(htmlStr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());
});
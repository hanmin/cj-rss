$(function(){
    //初始化显示前一天的数据 
    (function(){
        var date = new Date(new Date().getTime() - 24*1*60*60*1000);//前一天的日期
        var begin_date = date.format("yyyy-MM-dd");
        var end_date = begin_date;
        //console.log(begin_date);
        $("#date_plugin").val(begin_date);
        $("#imgtxt_date").html(begin_date);
        
        $.ajax({
            type: "get",
            url: 'weixinarticlereport/getarticletotal?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
            	var data = data;
            	if(data.returnFlag) {
                    var rep = data.rep;
                    //console.log(rep);
                    
                    //截取15个字作为标题
                    for (var i = 0; i < rep.length; i++) {
                        rep[i].title = cutTitle(rep[i].title);
                    }
                    
                    //动态添加DOM
                    var htmlList = "", htmlTemp = $("textarea").val();
                    rep.forEach(function(object) {
                        htmlList += htmlTemp.temp(object);
                    });
                    //console.log(htmlList);
                    
                    $("<div class='container mar-t20 tuwenCont'></div>").html(htmlList).appendTo($("#imgtxt_wrap"));
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    //日期控件选择日期
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "131px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
       $('#date_plugin').daterangepicker({
            singleDatePicker: true
       }, function(end){
            var begin_date = new Date(end).format("yyyy-MM-dd");
            var end_date = begin_date;
            
            $(".tuwenCont").remove();//清空数据
            $("#imgtxt_date").html(begin_date);

            $.ajax({
                type: "get",
                url: 'weixinarticlereport/getarticletotal?begin_date='+begin_date+'&end_date='+end_date,
                dataType: "json",
                success: function(data) {
                	var data = data;
                	if(data.returnFlag) {
                        var rep = data.rep;
                        //截取15个字作为标题
                        for (var i = 0; i < rep.length; i++) {
                            rep[i].title = cutTitle(rep[i].title);
                        }
                        //动态添加DOM
                        var htmlList = "", htmlTemp = $("textarea").val();
                        rep.forEach(function(object) {
                            htmlList += htmlTemp.temp(object);
                        });
                        //console.log(htmlList);
                        
                        $("<div class='container mar-t20 tuwenCont'></div>").html(htmlList).appendTo($("#imgtxt_wrap"));
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
           });
       });
    }());
});
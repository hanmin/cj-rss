$(function(){
	// 初始化显示当天及过去七天的数据
    (function(){
    	var date = new Date(new Date().getTime() - 24*1*60*60*1000);// 前一天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);// 前一天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " + end_date);
        
        $.ajax({
            type: "get",
            url: 'toutiao/getttwz?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if(data.returnFlag) {
                    var reps = data.returnMapsls;

                    for (var i = 0; i < reps.length; i++) {
                    	reps[i].redDate = date_substr(reps[i].redDate);// 截取日期
                        for(var j = 0;j < reps[i].returnls.length;j++){
                            reps[i].returnls[j].title = cutTitle(reps[i].returnls[j].title);// 截取20个字作为标题
                            reps[i].returnls[j].ydzhl = transformPer(reps[i].returnls[j].ydzhl);// 小数转换成百分比
                        }
                        
                        if(reps[i].returnls.length !== 0){
                        	// 动态添加日期栏
                            var htmlDate = "";
                            htmlDate += "<div class='title-link'>" ;
                            htmlDate += "<div class='container'>";
                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
                            htmlDate += "</div>";
                            htmlDate += "<span class='perch-line'></span>";
                            htmlDate += "</div>";

                            // 动态添加DOM
                            var htmlList = "", htmlTemp = $("textarea").val();
                            reps[i].returnls.forEach(function(object) {
                                htmlList += htmlTemp.temp(object);
                            });

                            var htmlStr = htmlDate + htmlList;
                            // console.log(htmlStr);
                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
                        }
                        else{
                        	// 动态添加日期栏
                            var htmlDate = "";
                            htmlDate += "<div class='title-link'>" ;
                            htmlDate += "<div class='container'>";
                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
                            htmlDate += "</div>";
                            htmlDate += "<span class='perch-line'></span>";
                            htmlDate += "</div>";

                            var htmlStr = htmlDate + "<div class='container mar-t10 mar-b10 f-13'>所选时间内无数据</div>";
                            // console.log(htmlStr);
                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
                        }
                    }
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    // 日期控件选择日期
    (function(){
    	// 日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "136px",
	            "left": 0,
	            "right": "auto"
	        }).toggle();
	    });
		
       $('#date_plugin').daterangepicker(null, function(start,end){
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");
            
            $(".toutiaoCont").remove();// 清空数据
            
            $.ajax({
                type: "get",
                url: 'toutiao/getttwz?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
                dataType: "json",
                success: function(data) {
                    var data = data;
                    if(data.returnFlag) {
                    	var reps = data.returnMapsls;

	                    for (var i = 0; i < reps.length; i++) {
	                    	reps[i].redDate = date_substr(reps[i].redDate);// 截取日期
	                        for(var j = 0;j < reps[i].returnls.length;j++){
	                            reps[i].returnls[j].title = cutTitle(reps[i].returnls[j].title);// 截取20个字作为标题
	                            reps[i].returnls[j].ydzhl = transformPer(reps[i].returnls[j].ydzhl);// 小数转换成百分比
	                        }
	                        
	                        if(reps[i].returnls.length !== 0){
	                        	// 动态添加日期栏
	                            var htmlDate = "";
	                            htmlDate += "<div class='title-link'>" ;
	                            htmlDate += "<div class='container'>";
	                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
	                            htmlDate += "</div>";
	                            htmlDate += "<span class='perch-line'></span>";
	                            htmlDate += "</div>";

	                            // 动态添加DOM
	                            var htmlList = "", htmlTemp = $("textarea").val();
	                            reps[i].returnls.forEach(function(object) {
	                                htmlList += htmlTemp.temp(object);
	                            });

	                            var htmlStr = htmlDate + htmlList;
	                            // console.log(htmlStr);
	                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
	                        }
	                        else{
	                        	// 动态添加日期栏
	                            var htmlDate = "";
	                            htmlDate += "<div class='title-link'>" ;
	                            htmlDate += "<div class='container'>";
	                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
	                            htmlDate += "</div>";
	                            htmlDate += "<span class='perch-line'></span>";
	                            htmlDate += "</div>";

	                            var htmlStr = htmlDate + "<div class='container mar-t10 mar-b10 f-13'>所选时间内无数据</div>";
	                            // console.log(htmlStr);
	                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
	                        }
	                    }
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
           });
       });
    }());
    
    // 阅读率排序
    (function(){
        $("#rankMenu").find("a").on("click", function(){
        	var _this = $(this); 
            var name = _this.html();// 阅读菜单子项名称
            var orderType = _this.attr("data-type");// 阅读菜单子项类型
            var ttDate = $("#date_plugin").val();// 获取日期
            // console.log(ttDate);
            var begin_date = ttDate.substr(0, 10);
            var end_date = ttDate.substr(13, 10);
            $("#rankBtn").html(name + "<span class='caret drop-icon'></span>");// 改变按钮文字
            
            $(".toutiaoCont").remove();// 清空数据

            $.ajax({
                type: "get",
                url: 'toutiao/getttwz?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType='+orderType,
                dataType: "json",
                success: function(data) {
                    var data = data;

                    if(data.returnFlag) {
                    	var reps = data.returnMapsls;

	                    for (var i = 0; i < reps.length; i++) {
	                    	reps[i].redDate = date_substr(reps[i].redDate);// 截取日期
	                        for(var j = 0;j < reps[i].returnls.length;j++){
	                            reps[i].returnls[j].title = cutTitle(reps[i].returnls[j].title);// 截取20个字作为标题
	                            reps[i].returnls[j].ydzhl = transformPer(reps[i].returnls[j].ydzhl);// 小数转换成百分比
	                        }
	                        
	                        if(reps[i].returnls.length !== 0){
	                        	// 动态添加日期栏
	                            var htmlDate = "";
	                            htmlDate += "<div class='title-link'>" ;
	                            htmlDate += "<div class='container'>";
	                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
	                            htmlDate += "</div>";
	                            htmlDate += "<span class='perch-line'></span>";
	                            htmlDate += "</div>";

	                            // 动态添加DOM
	                            var htmlList = "", htmlTemp = $("textarea").val();
	                            reps[i].returnls.forEach(function(object) {
	                                htmlList += htmlTemp.temp(object);
	                            });

	                            var htmlStr = htmlDate + htmlList;
	                            // console.log(htmlStr);
	                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
	                        }
	                        else{
	                        	// 动态添加日期栏
	                            var htmlDate = "";
	                            htmlDate += "<div class='title-link'>" ;
	                            htmlDate += "<div class='container'>";
	                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
	                            htmlDate += "</div>";
	                            htmlDate += "<span class='perch-line'></span>";
	                            htmlDate += "</div>";

	                            var htmlStr = htmlDate + "<div class='container mar-t10 mar-b10 f-13'>所选时间内无数据</div>";
	                            // console.log(htmlStr);
	                            $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
	                        }
	                    }
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
    
    // 全文检索
    (function(){
        $("#search_btn").on("click", function(){
            var searchResult = $.trim($("#search_result").val());// 全文检索输入值
            var ttDate = $("#date_plugin").val();// 获取日期
            // console.log(ttDate);
            var begin_date = ttDate.substr(0, 10);
            var end_date = ttDate.substr(13, 10);
            
            $(".toutiaoCont").remove();// 清空数据
            
            $.ajax({
                type: "get",
                url: 'toutiao/getttwz?begin_date='+begin_date+'&end_date='+end_date+'&keywords='+searchResult+'&orderType=',
                dataType: "json",
                success: function(data) {
                    var data = data;

                    if(data.returnFlag) {
                    	var reps = data.returnMapsls;

	                    for (var i = 0; i < reps.length; i++) {
	                    	reps[i].redDate = date_substr(reps[i].redDate);// 截取日期
	                        for(var j = 0;j < reps[i].returnls.length;j++){
	                            reps[i].returnls[j].title = cutTitle(reps[i].returnls[j].title);// 截取20个字作为标题
	                            reps[i].returnls[j].ydzhl = transformPer(reps[i].returnls[j].ydzhl);// 小数转换成百分比
	                        }

	                        if(reps[i].returnls.length !== 0){
	                        	// 动态添加日期栏
	                        	var htmlDate = "";
		                        htmlDate += "<div class='title-link'>" ;
		                        htmlDate += "<div class='container'>";
		                        htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].redDate + "</h4>";
		                        htmlDate += "</div>";
		                        htmlDate += "<span class='perch-line'></span>";
		                        htmlDate += "</div>";
		                        
		                        // 动态添加DOM
		                        var htmlList = "", htmlTemp = $("textarea").val();
		                        reps[i].returnls.forEach(function(object) {
		                            htmlList += htmlTemp.temp(object);
		                        });

		                        var htmlStr = htmlDate + htmlList;
		                        // console.log(htmlStr);
		                        $("<div class='data-wrap toutiaoCont'></div>").html(htmlStr).appendTo($("#toutiao_cont"));// 插入到稿件容器
	                        }
	                    }
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
});
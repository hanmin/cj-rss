$(function(){
    (function(){
        $.ajax({
            type: "get",
            url: "weixinhomereport/homereport",
            dataType: "json",
            success: function(data) {
                if (data.returnFlag) {
                	var data = data;
                	//微信实时数据
                	$.each(data.weixin, function(name, value){
                		$(".js_weixin").each(function(){
                			if(name === this.id){
                				$(this).html(value);
                    		}
                		});
                	});
                	//头条今日数据
                	$.each(data.toutiao, function(name, value){
                		$(".js_toutiao").each(function(){
                			if(name === this.id){
                				$(this).html(value);
                    		}
                		});
                	});
                	//微博实时指标
                	$.each(data.weibo, function(name, value){
                		$(".js_weibo").each(function(){
                			if(name === this.id){
                				$(this).html(value);
                    		}
                		});
                	});
                	//APP实时指标
                	$.each(data.app, function(name, value){
                		$(".js_app").each(function(){
                			if(name === this.id){
                				$(this).html(value);
                    		}
                		});
                	});
                	//APP频道数据
                	$.each(data.newsls, function(name, value){
                		$(".js_appCont").each(function(){
                			if(name === this.id){
                				$(this).html(value);
                    		}
                		});
                	});
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());
});
$(function(){
    //初始化显示当天的数据
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var begin_date = end_date;
        
        $("#date_plugin").val(end_date);

        $.ajax({
            type: "get",
            url: 'appreport/getappmanuscript?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if(data.returnFlag) {
                    var appVisitManuscript = data.appVisitManuscript;

                    //处理数据
                    $.each(appVisitManuscript, function(i, value){
                        value.ref_date = gainTime(value.ref_date);//日期
                        value.manuscript_title = cutTitle(appVisitManuscript[i].manuscript_title);//截取15个字作为标题
                    });

                    //动态添加DOM
                    var htmlList = "", htmlTemp = $("textarea").val();
                    appVisitManuscript.forEach(function(object) {
                        htmlList += htmlTemp.temp(object);
                    });

                    //console.log(htmlList);
                    $("#gaojian_cont").html(htmlList);//插入到稿件容器
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
        });
    }());

    //日期控件选择日期
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "78px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
       
       $('#date_plugin').daterangepicker({
            singleDatePicker: true
       }, function(end){
            var end_date = new Date(end).format("yyyy-MM-dd");
            var begin_date = end_date;
            
            $(".manuscript-cont").remove();//清空数据
            
            $.ajax({
                type: "get",
                url: 'appreport/getappmanuscript?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
                dataType: "json",
                success: function(data) {
                    var data = data;
                    
                    if(data.returnFlag) {
                        var appVisitManuscript = data.appVisitManuscript;

                        //处理数据
                        $.each(appVisitManuscript, function(i, value){
                            value.ref_date = gainTime(value.ref_date);//日期
                            value.manuscript_title = cutTitle(appVisitManuscript[i].manuscript_title);//截取15个字作为标题
                        });

                        //动态添加DOM
                        var htmlList = "", htmlTemp = $("textarea").val();
                        appVisitManuscript.forEach(function(object) {
                            htmlList += htmlTemp.temp(object);
                        });

                        //console.log(htmlList);
                        $("#gaojian_cont").html(htmlList);//插入到稿件容器
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
    //传播活性排序
    (function(){
        $("#rankMenu").find("a").on("click", function(){
            var _this = $(this); 
            var name = _this.html();//传播活性菜单子项名称
            var orderType = _this.attr("data-type");//传播活性菜单子项类型
            var begin_date = $("#date_plugin").val();//获取日期
            var end_date = begin_date;
            $("#rankBtn").html(name + "<span class='caret drop-icon'></span>");//改变按钮文字
            
            $(".manuscript-cont").remove();//清空数据

            $.ajax({
                type: "get",
                url: 'appreport/getappmanuscript?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType='+orderType,
                dataType: "json",
                success: function(data) {
                    var data = data;

                    if(data.returnFlag) {
                        var appVisitManuscript = data.appVisitManuscript;

                        //处理数据
                        $.each(appVisitManuscript, function(i, value){
                            value.ref_date = gainTime(value.ref_date);//日期
                            value.manuscript_title = cutTitle(appVisitManuscript[i].manuscript_title);//截取15个字作为标题
                        });

                        //动态添加DOM
                        var htmlList = "", htmlTemp = $("textarea").val();
                        appVisitManuscript.forEach(function(object) {
                            htmlList += htmlTemp.temp(object);
                        });

                        //console.log(htmlList);
                        $("#gaojian_cont").html(htmlList);//插入到稿件容器
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
    
    //全文检索
    (function(){
    	$("#search_btn").on("click", function(){
    		var searchResult = $.trim($("#search_result").val());//全文检索输入值
    		var begin_date = $("#date_plugin").val();//获取日期
            var end_date = begin_date;
    		
    		$(".manuscript-cont").remove();//清空数据
    		
    		$.ajax({
                type: "get",
                url: 'appreport/getappmanuscript?begin_date='+begin_date+'&end_date='+end_date+'&keywords='+searchResult+'&orderType=',
                dataType: "json",
                success: function(data) {
                    var data = data;

                    if(data.returnFlag) {
                        var appVisitManuscript = data.appVisitManuscript;

                        //处理数据
                        $.each(appVisitManuscript, function(i, value){
                            value.ref_date = gainTime(value.ref_date);//日期
                            value.manuscript_title = cutTitle(appVisitManuscript[i].manuscript_title);//截取15个字作为标题
                        });

                        //动态添加DOM
                        var htmlList = "", htmlTemp = $("textarea").val();
                        appVisitManuscript.forEach(function(object) {
                            htmlList += htmlTemp.temp(object);
                        });

                        //console.log(htmlList);
                        $("#gaojian_cont").html(htmlList);//插入到稿件容器
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
    	});
    }());
});
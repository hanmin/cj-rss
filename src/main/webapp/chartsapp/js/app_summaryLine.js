$(function(){
    //初始化显示当天及过去七天的数据
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " +end_date);
        
        $.ajax({
            type: "get",
            url: 'appreport/getappcontant?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                if (data.returnFlag) {
                    var appcontantls = data.appcontantls;
                    appcontantls.reverse();//颠倒json数据数组

                    //处理数据
                    $.each(appcontantls, function(i, value){
                        value.refDate = date_substr(value.refDate);//日期
                    });

                    var dateArr = [],
                        fg_arr = [],
                        bl_arr = [],
                        dy_arr = [],
                        qx_arr = [],
                        dylm_arr = [],
                        qxlm_arr = [],
                        dyjz_arr = [],
                        qxjz_arr = [];

                    for (var i = 0; i < appcontantls.length; i++) {
                        dateArr.push(appcontantls[i].refDate);//日期
                        fg_arr.push(appcontantls[i].manuscript_type1);//发稿数
                        bl_arr.push(appcontantls[i].manuscript_type2);//爆料数
                        dy_arr.push(appcontantls[i].appVisitColumnSub_CSubCount);//订阅/取消栏目
                        qx_arr.push(appcontantls[i].appVisitReporterSub_CSubCount);//订阅/取消记者
                    }

                    //拆分订阅栏目和取消栏目
                    $.each(dy_arr, function(i,value){
                      dylm_arr.push(splitStr(value)[0]);
                      qxlm_arr.push(splitStr(value)[1]);
                    });
                    //拆分订阅记者和取消记者
                    $.each(qx_arr, function(i,value){
                      dyjz_arr.push(splitStr(value)[0]);
                      qxjz_arr.push(splitStr(value)[1]);
                    });

                    //画折线
                    DrawLineEChart("fagao", "发稿数", dateArr, fg_arr);
                    DrawLineEChart("baoliao", "爆料数", dateArr, bl_arr);
                    DoubleLineEChart("dylm", "订阅栏目", "取消栏目", dateArr, dylm_arr, qxlm_arr);
                    DoubleLineEChart("dyjz", "订阅记者", "取消记者", dateArr, dyjz_arr, qxjz_arr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
                myChart.hideLoading();
            }
       });
    }());
    
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "121px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $.ajax({
                 type: "get",
                 url: 'appreport/getappcontant?begin_date='+begin_date+'&end_date='+end_date,
                 dataType: "json",
                 success: function(data) {
                    var data = data;
                    if (data.returnFlag) {
                        var appcontantls = data.appcontantls;
                        appcontantls.reverse();//颠倒json数据数组

                        //处理数据
                        $.each(appcontantls, function(i, value){
                            value.refDate = date_substr(value.refDate);//日期
                        });

                        var dateArr = [],
                            fg_arr = [],
                            bl_arr = [],
                            dy_arr = [],
                            qx_arr = [],
                            dylm_arr = [],
                            qxlm_arr = [],
                            dyjz_arr = [],
                            qxjz_arr = [];

                        for (var i = 0; i < appcontantls.length; i++) {
                            dateArr.push(appcontantls[i].refDate);//日期
                            fg_arr.push(appcontantls[i].manuscript_type1);//发稿数
                            bl_arr.push(appcontantls[i].manuscript_type2);//爆料数
                            dy_arr.push(appcontantls[i].appVisitColumnSub_CSubCount);//订阅/取消栏目
                            qx_arr.push(appcontantls[i].appVisitReporterSub_CSubCount);//订阅/取消记者
                        }

                        //拆分订阅栏目和取消栏目
                        $.each(dy_arr, function(i,value){
                          dylm_arr.push(splitStr(value)[0]);
                          qxlm_arr.push(splitStr(value)[1]);
                        });
                        //拆分订阅记者和取消记者
                        $.each(qx_arr, function(i,value){
                          dyjz_arr.push(splitStr(value)[0]);
                          qxjz_arr.push(splitStr(value)[1]);
                        });
                        
                        //画折线
                        DrawLineEChart("fagao", "发稿数", dateArr, fg_arr);
                        DrawLineEChart("baoliao", "爆料数", dateArr, bl_arr);
                        DoubleLineEChart("dylm", "订阅栏目", "取消栏目", dateArr, dylm_arr, qxlm_arr);
                        DoubleLineEChart("dyjz", "订阅记者", "取消记者", dateArr, dyjz_arr, qxjz_arr);
                    }
                 },
                 error: function(errorMsg) {
                     alert("请求数据失败!");
                     myChart.hideLoading();
                 }
            });
        });
    }());
});

function DrawLineEChart(id, title, dateArr, dataArr){
    var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis'
        },
        calculable : true,
        grid: {
            x: 50,
            y: 10,
            x2: 18,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title,
                type:'line',
                stack: '总量',
                data: dataArr
            }
        ]
    };
                    
    myChart.setOption(option);
};

function DoubleLineEChart(id, title1, title2, dateArr, dyArr, qxArr){
    var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis'
        },
        calculable : true,
        grid: {
            x: 50,
            y: 10,
            x2: 18,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title1,
                type:'line',
                stack: '总量',
                data: dyArr
            },
            {
                name: title2,
                type:'line',
                stack: '总量',
                data: qxArr
            }
        ]
    };
                    
    myChart.setOption(option);
};
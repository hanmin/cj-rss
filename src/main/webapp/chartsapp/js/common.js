//快速点击，消除移动端点击延迟的300ms时间
$(function(){
	FastClick.attach(document.body);
});

//日期格式化函数+调用方法
Date.prototype.format = function(format){
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
        "S" : this.getMilliseconds() //millisecond
    };
    if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o){
        if(new RegExp("("+ k +")").test(format))
            format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] :("00"+ o[k]).substr((""+ o[k]).length));
    }
    return format;
};

//截取日期格式，去掉年份
function date_substr(date){
    return date.substr(5);
}

//截取日期格式，去掉年月日，保留时间
function gainTime(date){
    return date.substr(11);
}

//以"/"分割字符串
function splitStr(str){
  return str.split("/");
}

//截取文本字符串,多行文本溢出显示省略号
function cutTitle(title){
    return title.substr(0,20) + "...";
}

//判断增长率的数值  空/正/负
function rate_judge(rate){
    if(rate === ""){
        return rate + "--";
    }
    else{
        if(rate > 0){
            return "+" + rate;
        }
        else{
            return rate;
        }
    }
}

//小数转换成百分比
function transformPer(decimal){
    return parseInt(decimal*100) + "%";
}

//一个固定的基于字符串原型的扩展方法(js模板)
String.prototype.temp = function(obj) {
    return this.replace(/\$\w+\$/gi, function(matchs) {
        var returns = obj[matchs.replace(/\$/g, "")];       
        return (returns + "") == "undefined"? "": returns;
    });
};
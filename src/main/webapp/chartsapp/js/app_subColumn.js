$(function(){
    //初始化显示当天及前七天
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        var sub_id = $("#sub_id").val();//链接地址id
        
        $("#date_plugin").val(begin_date + " - " +end_date);
        
        $.ajax({
            type: "get",
            url: 'appreport/getappcloumn?begin_date='+begin_date+'&end_date='+end_date+'&columnId='+sub_id,
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if (data.returnFlag) {
                    var columnls = data.columnls;

                    for (var i = 0; i < columnls.length; i++) {
                        var repobjls = columnls[i].repobjls;//子栏目数据
                        var name = columnls[i].name;//子栏目名称

                        var dateArr = [],
                            count_arr = [],
                            readCount_arr = [],
                            cblv_arr = [];

                        for(var j = 0;j < repobjls.length;j++) {
                            //处理日期   
                            var refDate = repobjls[j].refDate;
                            refDate = date_substr(refDate);

                            dateArr.push(refDate);//日期
                            count_arr.push(repobjls[j].count);//发稿数
                            readCount_arr.push(repobjls[j].readCount);//阅读量
                            cblv_arr.push(repobjls[j].cblv);//传播活性
                        }

                        //动态创建图表容器
                        var htmlLine = "";
                        htmlLine += "<h5 class='stat-title text-center'>"+name+"</h5>";
                        htmlLine += "<div id='column_line"+i+"' style='height:200px'></div>";
                        console.log(htmlLine);

                        $("<div class='subColumn-line'></div>").html(htmlLine).appendTo($("#subColumn_cont"));//插入到子栏目容器

                        //画折线
                        DrawLineEChart("column_line"+i, "发稿数", "阅读量", "传播活性", dateArr, count_arr, readCount_arr, cblv_arr);
                    }
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
                myChart.hideLoading();
            }
       });
    }());
    
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "131px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");
            var sub_id = $("#sub_id").val();//链接地址id
            
            $(".subColumn-line").remove();//清空数据
            
            $.ajax({
                 type: "get",
                 url: 'appreport/getappcloumn?begin_date='+begin_date+'&end_date='+end_date+'&columnId='+sub_id,
                 dataType: "json",
                 success: function(data) {
                    var data = data;
                    
                    if (data.returnFlag) {
                        var columnls = data.columnls;

                        for (var i = 0; i < columnls.length; i++) {
                            var repobjls = columnls[i].repobjls;//子栏目数据
                            var name = columnls[i].name;//子栏目名称

                            var dateArr = [],
                                count_arr = [],
                                readCount_arr = [],
                                cblv_arr = [];

                            for(var j = 0;j < repobjls.length;j++) {
                                //处理日期   
                                var refDate = repobjls[j].refDate;
                                refDate = date_substr(refDate);

                                dateArr.push(refDate);//日期
                                count_arr.push(repobjls[j].count);//发稿数
                                readCount_arr.push(repobjls[j].readCount);//阅读量
                                cblv_arr.push(repobjls[j].cblv);//传播活性
                            }

                            //动态创建图表容器
                            var htmlLine = "";
                            htmlLine += "<h5 class='stat-title text-center'>"+name+"</h5>";
                            htmlLine += "<div id='column_line"+i+"' style='height:200px'></div>";
                            console.log(htmlLine);

                            $("<div class='subColumn-line'></div>").html(htmlLine).appendTo($("#subColumn_cont"));//插入到子栏目容器

                            //画折线
                            DrawLineEChart("column_line"+i, "发稿数", "阅读量", "传播活性", dateArr, count_arr, readCount_arr, cblv_arr);
                        }
                    }
                 },
                 error: function(errorMsg) {
                     alert("请求数据失败!");
                     myChart.hideLoading();
                 }
            });
        });
    }());
});

function DrawLineEChart(id, title1, title2, title3, dateArr, dataArr1, dataArr2, dataArr3){
    var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:['发稿数','阅读量','传播活性']
        },
        calculable : true,
        grid: {
            x: 50,
            y: 30,
            x2: 18,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title1,
                type:'line',
                stack: '总量',
                data: dataArr1
            },
            {
                name: title2,
                type:'line',
                stack: '总量',
                data: dataArr2
            },
            {
                name: title3,
                type:'line',
                stack: '总量',
                data: dataArr3
            }
        ]
    };
                    
    myChart.setOption(option);
};
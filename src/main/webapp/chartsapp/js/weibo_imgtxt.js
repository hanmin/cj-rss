$(function(){
    //初始化显示当天及过去七天的数据
    (function(){
        var date = new Date(new Date().getTime() - 24*1*60*60*1000);//前一天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//前一天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " + end_date);
        
        $.ajax({
            type: "get",
            url: 'weiboreport/getweiboimagetext?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                if(data.returnFlag) {
                    var reps = data.reps;

                    for (var i = 0; i < reps.length; i++) {
                        for(var j = 0;j < reps[i].objs.length;j++){
                            reps[i].objs[j].title = cutTitle(reps[i].objs[j].title);//截取20个字作为标题
                            //console.log(reps[i].objs[j].title);
                        }
                        
                        if(reps[i].objs.length !== 0){
                        	//动态添加日期栏
                            var htmlDate = "";
                            htmlDate += "<div class='title-link'>" ;
                            htmlDate += "<div class='container'>";
                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].refDate + "</h4>";
                            htmlDate += "</div>";
                            htmlDate += "<span class='perch-line'></span>";
                            htmlDate += "</div>";

                            //动态添加DOM
                            var htmlList = "", htmlTemp = $("textarea").val();
                            reps[i].objs.forEach(function(object) {
                                htmlList += htmlTemp.temp(object);
                            });

                            var htmlStr = htmlDate + htmlList;
                            //console.log(htmlStr);
                            $("<div class='data-wrap blogCont'></div>").html(htmlStr).appendTo($("#bowen_cont"));//插入到博文容器
                        }
                        else{
                        	//动态添加日期栏
                            var htmlDate = "";
                            htmlDate += "<div class='title-link'>" ;
                            htmlDate += "<div class='container'>";
                            htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].refDate + "</h4>";
                            htmlDate += "</div>";
                            htmlDate += "<span class='perch-line'></span>";
                            htmlDate += "</div>";

                            var htmlStr = htmlDate + "<div class='container mar-t10 mar-b10 f-13'>所选时间内无数据</div>";
                            //console.log(htmlStr);
                            $("<div class='data-wrap blogCont'></div>").html(htmlStr).appendTo($("#bowen_cont"));//插入到博文容器
                        }
                    }
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    //日期控件选择日期
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "131px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
       $('#date_plugin').daterangepicker(null, function(start,end){
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");
            
            $(".blogCont").remove();//清空数据
            
            $.ajax({
                type: "get",
                url: 'weiboreport/getweiboimagetext?begin_date='+begin_date+'&end_date='+end_date,
                dataType: "json",
                success: function(data) {
                    var data = data;
                    if(data.returnFlag) {
                        var reps = data.reps;

                        for (var i = 0; i < reps.length; i++) {
                            for(var j = 0;j < reps[i].objs.length;j++){
                                reps[i].objs[j].title = cutTitle(reps[i].objs[j].title);//截取15个字作为标题
                                //console.log(reps[i].objs[j].title);
                            }
                            
                            if(reps[i].objs.length !== 0){
                            	//动态添加日期栏
                                var htmlDate = "";
                                htmlDate += "<div class='title-link'>" ;
                                htmlDate += "<div class='container'>";
                                htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].refDate + "</h4>";
                                htmlDate += "</div>";
                                htmlDate += "<span class='perch-line'></span>";
                                htmlDate += "</div>";

                                //动态添加DOM
                                var htmlList = "", htmlTemp = $("textarea").val();
                                reps[i].objs.forEach(function(object) {
                                    htmlList += htmlTemp.temp(object);
                                });

                                var htmlStr = htmlDate + htmlList;
                                //console.log(htmlStr);
                                $("<div class='data-wrap blogCont'></div>").html(htmlStr).appendTo($("#bowen_cont"));//插入到博文容器
                            }
                            else{
                            	//动态添加日期栏
                                var htmlDate = "";
                                htmlDate += "<div class='title-link'>" ;
                                htmlDate += "<div class='container'>";
                                htmlDate += "<h4 class='f-16 pad-l10'>" + reps[i].refDate + "</h4>";
                                htmlDate += "</div>";
                                htmlDate += "<span class='perch-line'></span>";
                                htmlDate += "</div>";

                                var htmlStr = htmlDate + "<div class='container mar-t10 mar-b10 f-13'>所选时间内无数据</div>";
                                //console.log(htmlStr);
                                $("<div class='data-wrap blogCont'></div>").html(htmlStr).appendTo($("#bowen_cont"));//插入到博文容器
                            }
                        }
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
           });
       });
    }());
});
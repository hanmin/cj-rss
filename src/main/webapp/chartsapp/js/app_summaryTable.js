$(function(){
	//初始化显示当天及过去七天的数据
    (function(){
        var date = new Date();//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " +end_date);
        
        $.ajax({
            type: "get",
            url: 'appreport/getappcontant?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                if (data.returnFlag) {
                    var data = data;
                    var appcontantls = data.appcontantls;
                    
                    //处理日期
                    $.each(appcontantls, function(i, value){
                        value.refDate = date_substr(value.refDate);
                    });
                    
                    var htmlStr = "";
                    for (var i = 0; i < appcontantls.length; i++) {
                        htmlStr += "<ul class='table-item'>";
                        htmlStr += "<li class='f-15'>" + appcontantls[i].refDate + "</li>" 
                                          + "<li>" + appcontantls[i].manuscript_type1 + "</li>"
                                          + "<li>" + appcontantls[i].manuscript_type2 + "</li>"
                                          + "<li>" + appcontantls[i].appVisitColumnSub_CSubCount + "</li>"
                                          + "<li>" + appcontantls[i].appVisitReporterSub_CSubCount + "</li>";
                        htmlStr += "</ul>";
                    }
                    $("#table_cont").append(htmlStr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "121px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $(".table-item").remove();//清空数据
            
            $.ajax({
                type: "get",
                url: 'appreport/getappcontant?begin_date='+begin_date+'&end_date='+end_date,
                dataType: "json",
                success: function(data) {
                    var data = data;
                    if (data.returnFlag) {
                        var data = data;
                        var appcontantls = data.appcontantls;
                        
                        //处理日期
                        $.each(appcontantls, function(i, value){
                            value.refDate = date_substr(value.refDate);
                        });
                        
                        var htmlStr = "";
                        for (var i = 0; i < appcontantls.length; i++) {
                            htmlStr += "<ul class='table-item'>";
                            htmlStr += "<li class='f-15'>" + appcontantls[i].refDate + "</li>" 
                                              + "<li>" + appcontantls[i].manuscript_type1 + "</li>"
                                              + "<li>" + appcontantls[i].manuscript_type2 + "</li>"
                                              + "<li>" + appcontantls[i].appVisitColumnSub_CSubCount + "</li>"
                                              + "<li>" + appcontantls[i].appVisitReporterSub_CSubCount + "</li>";
                            htmlStr += "</ul>";
                        }
                        $("#table_cont").append(htmlStr);
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
           });
        });
    }());
});
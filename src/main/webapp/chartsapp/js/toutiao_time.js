$(function(){
	//初始化显示当天及过去七天的数据
    (function(){
    	var date = new Date(new Date().getTime() - 24*1*60*60*1000);// 前一天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " +end_date);
        
        $.ajax({
            type: "get",
            url: 'toutiao/getttwzt?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if (data.returnFlag) {
                    var data = data;
                    var returnMapsls = data.returnMapsls;
                    
                    //处理日期
                    $.each(returnMapsls, function(i, value){
                        value.ref_date = date_substr(value.ref_date);//截取日期
                    });
                    
                    var htmlStr = "";
                    for (var i = 0; i < returnMapsls.length; i++) {
                        htmlStr += "<ul class='table-item'>";
                        htmlStr += "<li class='f-15'>" + returnMapsls[i].ref_date + "</li>" 
                                          + "<li>" + returnMapsls[i].count + "</li>"
                                          + "<li>" + returnMapsls[i].recommendedCount + "</li>"
                                          + "<li>" + returnMapsls[i].readCount + "</li>"
                                          + "<li>" + returnMapsls[i].commentCount + "</li>"
                                          + "<li>" + returnMapsls[i].collectionCount + "</li>"
                                          + "<li>" + returnMapsls[i].forwardingCount + "</li>";
                        htmlStr += "</ul>";
                    }
                    $("#toutiao_cont").append(htmlStr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
            }
       });
    }());

    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "136px",
	            "left": 0,
	            "right": "auto"
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $(".table-item").remove();//清空数据
            
            $.ajax({
                type: "get",
                url: 'toutiao/getttwzt?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType=',
                dataType: "json",
                success: function(data) {
                    var data = data;
                    
                    if (data.returnFlag) {
                        var data = data;
                        var returnMapsls = data.returnMapsls;
                        
                        //处理日期
                        $.each(returnMapsls, function(i, value){
                            value.ref_date = date_substr(value.ref_date);//截取日期
                        });
                        
                        var htmlStr = "";
                        for (var i = 0; i < returnMapsls.length; i++) {
                            htmlStr += "<ul class='table-item'>";
                            htmlStr += "<li class='f-15'>" + returnMapsls[i].ref_date + "</li>" 
                                              + "<li>" + returnMapsls[i].count + "</li>"
                                              + "<li>" + returnMapsls[i].recommendedCount + "</li>"
                                              + "<li>" + returnMapsls[i].readCount + "</li>"
                                              + "<li>" + returnMapsls[i].commentCount + "</li>"
                                              + "<li>" + returnMapsls[i].collectionCount + "</li>"
                                              + "<li>" + returnMapsls[i].forwardingCount + "</li>";
                            htmlStr += "</ul>";
                        }
                        $("#toutiao_cont").append(htmlStr);
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
           });
        });
    }());
    // 阅读排序
    (function(){
        $("#rankMenu").find("a").on("click", function(){
        	var _this = $(this); 
            var name = _this.html();// 阅读菜单子项名称
            var orderType = _this.attr("data-type");// 阅读菜单子项类型
            var ttDate = $("#date_plugin").val();// 获取日期
            // console.log(ttDate);
            var begin_date = ttDate.substr(0, 10);
            var end_date = ttDate.substr(13, 10);
            $("#rankBtn").html(name + "<span class='caret drop-icon'></span>");// 改变按钮文字
            
            $(".table-item").remove();// 清空数据

            $.ajax({
                type: "get",
                url: 'toutiao/getttwzt?begin_date='+begin_date+'&end_date='+end_date+'&keywords=&orderType='+orderType,
                dataType: "json",
                success: function(data) {
                    var data = data;

                    if (data.returnFlag) {
                        var data = data;
                        var returnMapsls = data.returnMapsls;
                        
                        //处理日期
                        $.each(returnMapsls, function(i, value){
                            value.ref_date = date_substr(value.ref_date);//截取日期
                        });
                        
                        var htmlStr = "";
                        for (var i = 0; i < returnMapsls.length; i++) {
                            htmlStr += "<ul class='table-item'>";
                            htmlStr += "<li class='f-15'>" + returnMapsls[i].ref_date + "</li>" 
                                              + "<li>" + returnMapsls[i].count + "</li>"
                                              + "<li>" + returnMapsls[i].recommendedCount + "</li>"
                                              + "<li>" + returnMapsls[i].readCount + "</li>"
                                              + "<li>" + returnMapsls[i].commentCount + "</li>"
                                              + "<li>" + returnMapsls[i].collectionCount + "</li>"
                                              + "<li>" + returnMapsls[i].forwardingCount + "</li>";
                            htmlStr += "</ul>";
                        }
                        $("#toutiao_cont").append(htmlStr);
                    }
                },
                error: function(errorMsg) {
                    alert("请求数据失败!");
                }
            });
        });
    }());
});
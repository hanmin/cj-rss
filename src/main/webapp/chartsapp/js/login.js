$(function(){
	(function(){
		//后台判断，微信登录
		$("#login_btn").on("click", function(){
			var account = $("#account").val();//账户名
			//console.log(account);
			$.ajax({
	            type: "get",
	            url: 'applogin/doapplogin?account='+account+'&sysId=1',
	            dataType: "json",
	            success: function(data) {
	                if (data.returnFlag == "0") {
	                	window.location.href = "cj_index";
	                }
	                else{
	                	//弹出框提示
	                	var myModal = $('#myModal');
	                    var timer = setTimeout(function () {
	                        var _modal = myModal.find(".modal-dialog");
	                        _modal.animate({'margin-top': parseInt(($(window).height()-_modal.height())/2)}, "slow")
	                    },200);
	                    myModal.modal();
	                }
	            },
	            error: function(errorMsg) {
	                alert("请求数据失败!");
	            }
	       });
		});
	}());
});
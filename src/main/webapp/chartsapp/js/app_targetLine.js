$(function(){
    //初始化显示当天及过去七天的数据
    (function(){
        var date = new Date(new Date().getTime() - 24*1*60*60*1000);//当天的日期
        var end_date = date.format("yyyy-MM-dd");
        var date2 = new Date(date.getTime() - 24*6*60*60*1000);//当天之前的第七天的日期
        var begin_date = date2.format("yyyy-MM-dd");
        
        $("#date_plugin").val(begin_date + " - " +end_date);
        
        $.ajax({
            type: "get",
            url: 'appreport/getappvisitcount?begin_date='+begin_date+'&end_date='+end_date,
            dataType: "json",
            success: function(data) {
                var data = data;
                
                if (data.returnFlag) {
                    var reps = data.reps;
                    reps.reverse();

                    //处理数据
                    $.each(reps, function(i, value){
                        value.redDate = date_substr(value.redDate);//日期
                        value.app_download_rate = rate_judge(value.app_download_rate);//下载率
                        value.app_register_rate = rate_judge(value.app_register_rate);//注册率
                        value.app_install_rate = rate_judge(value.app_install_rate);//安装率
                        value.app_active_rate = rate_judge(value.app_active_rate);//日活率
                        value.app_unload_rate = rate_judge(value.app_unload_rate);//卸载率
                    });

                    var dateArr = [],
                        down_arr = [],
                        regist_arr = [],
                        install_arr = [],
                        active_arr = [],
                        unload_arr = [],
                        downRate_arr = [],
                        registRate_arr = [],
                        installRate_arr = [],
                        activeRate_arr = [],
                        unloadRate_arr = [];

                    for (var i = 0; i < reps.length; i++) {
                        dateArr.push(reps[i].redDate);
                        down_arr.push(reps[i].appDownload);//下载
                        install_arr.push(reps[i].appInstall);//安装
                        regist_arr.push(reps[i].appRegister);//注册
                        active_arr.push(reps[i].appActive);//日活
                        unload_arr.push(reps[i].appUnload);//卸载
                        downRate_arr.push(reps[i].app_download_rate);//下载率
                        installRate_arr.push(reps[i].app_install_rate);//安装率
                        registRate_arr.push(reps[i].app_register_rate);//注册率
                        activeRate_arr.push(reps[i].app_active_rate);//日活率
                        unloadRate_arr.push(reps[i].app_unload_rate);//卸载率
                    }

                    //画折线
                    DrawLineEChart("download", "下载", dateArr, down_arr, downRate_arr);
                    DrawLineEChart("install", "安装", dateArr, install_arr, installRate_arr);
                    DrawLineEChart("register", "注册", dateArr, regist_arr, registRate_arr);
                    DrawLineEChart("dayActive", "日活", dateArr, active_arr, activeRate_arr);
                    DrawLineEChart("uninstall", "卸载", dateArr, unload_arr, unloadRate_arr);
                }
            },
            error: function(errorMsg) {
                alert("请求数据失败!");
                myChart.hideLoading();
            }
       });
    }());
    
    (function(){
    	//日期下拉图标弹出日期控件
		$("#dropdown_date").on("click", function(){
	        $(".daterangepicker").css({
	            "top": "121px",
	            "left": "auto",
	            "right": 0
	        }).toggle();
	    });
		
        //日期控件选择日期
        $('#date_plugin').daterangepicker(null, function(start, end) {
            var begin_date = new Date(start).format("yyyy-MM-dd");
            var end_date = new Date(end).format("yyyy-MM-dd");

            $.ajax({
                 type: "get",
                 url: 'appreport/getappvisitcount?begin_date='+begin_date+'&end_date='+end_date,
                 dataType: "json",
                 success: function(data) {
                    var data = data;
                    
                    if (data.returnFlag) {
                        var reps = data.reps;
                        reps.reverse();

                        //处理数据
                        $.each(reps, function(i, value){
                            value.redDate = date_substr(value.redDate);//日期
                            value.app_download_rate = rate_judge(value.app_download_rate);//下载率
                            value.app_register_rate = rate_judge(value.app_register_rate);//注册率
                            value.app_install_rate = rate_judge(value.app_install_rate);//安装率
                            value.app_active_rate = rate_judge(value.app_active_rate);//日活率
                            value.app_unload_rate = rate_judge(value.app_unload_rate);//卸载率
                        });

                        var dateArr = [],
                            down_arr = [],
                            regist_arr = [],
                            install_arr = [],
                            active_arr = [],
                            unload_arr = [],
                            downRate_arr = [],
                            registRate_arr = [],
                            installRate_arr = [],
                            activeRate_arr = [],
                            unloadRate_arr = [];

                        for (var i = 0; i < reps.length; i++) {
                            dateArr.push(reps[i].redDate);
                            down_arr.push(reps[i].appDownload);//下载
                            install_arr.push(reps[i].appInstall);//安装
                            regist_arr.push(reps[i].appRegister);//注册
                            active_arr.push(reps[i].appActive);//日活
                            unload_arr.push(reps[i].appUnload);//卸载
                            downRate_arr.push(reps[i].app_download_rate);//下载率
                            installRate_arr.push(reps[i].app_install_rate);//安装率
                            registRate_arr.push(reps[i].app_register_rate);//注册率
                            activeRate_arr.push(reps[i].app_active_rate);//日活率
                            unloadRate_arr.push(reps[i].app_unload_rate);//卸载率
                        }

                        //画折线
                        DrawLineEChart("download", "下载", dateArr, down_arr, downRate_arr);
                        DrawLineEChart("install", "安装", dateArr, install_arr, installRate_arr);
                        DrawLineEChart("register", "注册", dateArr, regist_arr, registRate_arr);
                        DrawLineEChart("dayActive", "日活", dateArr, active_arr, activeRate_arr);
                        DrawLineEChart("uninstall", "卸载", dateArr, unload_arr, unloadRate_arr);
                    }
                 },
                 error: function(errorMsg) {
                     alert("请求数据失败!");
                     myChart.hideLoading();
                 }
            });
        });
    }());
});

function DrawLineEChart(id, title, dateArr, dataArr, rateArr){
    var myChart = echarts.init(document.getElementById(id));

    var option = {
        tooltip : {
            trigger: 'axis',
            formatter: function (params,ticket,callback) {
                //console.log(ticket);
                var result = "";
                var res = params[0].name + '<br/>' + params[0].seriesName + ' : ' + params[0].value;
                var re = '<br/>' + '增长率' + ' : ' + rateArr[ticket.split(":")[1]];
                result = res + re;
                setTimeout(function (){
                    // 仅为了模拟异步回调
                    callback(ticket, result);
                }, 0)
            }
        },
        calculable : true,
        grid: {
            x: 50,
            y: 10,
            x2: 18,
            y2: 40
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : dateArr
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: title,
                type:'line',
                stack: '总量',
                data: dataArr
            }
        ]
    };
                    
    myChart.setOption(option);
};